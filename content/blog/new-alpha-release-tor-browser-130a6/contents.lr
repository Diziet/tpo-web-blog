title: New Alpha Release: Tor Browser 13.0a6 (Android, Windows, macOS, Linux)
---
pub_date: 2023-09-29
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0a6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0a6 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a6/).

This release backports [important security fixes](https://www.mozilla.org/en-US/security/advisories/mfsa2023-44/) from Firefox 115.3.1. There were no Android-specific security backports from Firefox 115.3.0.

## Major Changes

This is our sixth alpha release in the 13.0 series which represents a transition from Firefox 102-esr to Firefox 115-esr. It is also our first release-candidate build for 13.0 stable. If you find any issues, please report them on our [gitlab](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/new) or on the [Tor Project forum](https://forum.torproject.org/c/feedback/tor-browser-alpha-feedback/6).

We have completed our annual esr transition audit, and the final reports should be available in our [tor-browser-spec repository](https://gitlab.torproject.org/tpo/applications/tor-browser-spec/) in the audits directory. All of the interesting upstream patches have either been disabled or found to not be a problem for us on closer inspection.

### Build Output Naming Updates

As a reminder from the [13.0a3](https://blog.torproject.org/new-alpha-release-tor-browser-130a3) release post, we have made the naming scheme for all of our build outputs mutually consistent. If you are a downstream packager or in some other way download Tor Browser artifacts in scripts or automation, you will have a bit more work to do beyond bumping the version number once the 13.0 alpha stabilizes. All of our current build outputs can be found in the [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a6/)

## Full changelog

We would like to thank volunteer contributor guest475646844 for their fix for [tor-browser#41165](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41165). If you would like to contribute, our issue tracker can be found [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/boards/1243#Platform).

The full changelog since [Tor Browser 13.0a5](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated tor to 0.4.8.7
  - Updated Translations
  - [Bug tor-browser#42139](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42139): Backport security fixes from Firefox 115.3.1 to 115.3.0
- Windows + macOS + Linux
  - [Bug tor-browser#40899](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40899): Fix all of our usage of `app.support.baseURL`
  - [Bug tor-browser#41165](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41165): Crash with debug assertions enabled
  - [Bug tor-browser#41910](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41910): Support link regression in 115.1.0esr
  - [Bug tor-browser#42100](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42100): Connect Assist dropdown text not centered
  - [Bug tor-browser#42129](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42129): Disable the Tor restart prompt if shouldStartAndOwnTor is falses
  - [Bug tor-browser#42131](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42131): Tor Browser 13.0a5 does not track circuits created before Tor Browser started
  - [Bug tor-browser#42132](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42132): The new control port handling in Tor Browser 13 breaks a Tails security feature
- Windows
  - [Bug tor-browser-build#40954](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40954): Implement Windows installer icons
  - [Bug tor-browser#42087](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42087): Implement Windows application icons
- Android
  - [Bug tor-browser#42023](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42023): TB13.0a2 android: remove FF what's new
  - [Bug tor-browser#42114](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42114): Disable Allow sharing current tab URL from Android's Recents screen in private browsing mode
  - [Bug tor-browser#42115](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42115): Enhanced Tracking Protection can still be enabled
  - [Bug tor-browser#42133](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42133): Remove "Total Cookie Protection" popup
  - [Bug tor-browser#42134](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42134): Remove Android icon shortcuts
- Build System
  - All Platforms
    - [Bug tor-browser-build#40741](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40741): Update browser and tor-android-service projects to pull data from pt_config.json
    - [Bug tor-browser-build#40957](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40957): Expired subkey warning on Tor Browser GPG verification
    - [Bug tor-browser#42130](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42130): Add support for specifying the branch in `tb-dev rebase-on-default`
  - Android
    - [Bug tor-browser-build#40963](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40963): Tor Browesr 13.0 torbrowser-testbuild-android* targets fail to build
