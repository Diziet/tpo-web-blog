title: Putting Censorship Circumvention to the Test: Security Audit Findings
---
author: pavel
---
pub_date: 2023-10-25
---
categories: 

circumvention
reports
applications
---
weight: 1
---
summary: To ensure that our technologies are resilient against threats and attacks, we've tasked [Cure53](https://cure53.de/) to perform a security audit of Tor Browser and other tools related to censorship circumvention. You can view the full report and more about their findings in this blog post.
---
body:

Helping users bypass internet censorship and ensuring their online safety and security is at the core of everything we do. To protect the communities we serve, we want to ensure that our technologies are resilient against threats and attacks. To put our efforts to the test, we've tasked [Cure53](https://cure53.de/) to perform a security audit of Tor Browser and other tools related to censorship circumvention.

Security audits are important, they uncover blind spots, peel back assumptions, and show us ways to improve our overall security posture. A series of penetration tests and code audits were performed specifically targeting methods by which users connect to bridges in [Tor Browser](https://www.torproject.org/download/), as well as [OONI Probe](https://ooni.org/), [rdsys](https://gitlab.torproject.org/tpo/anti-censorship/rdsys/), [BridgeDB](https://gitlab.torproject.org/tpo/anti-censorship/bridgedb) and [Conjure](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/conjure).

We invite you to read the [full report](TTP-01-report.pdf), the testing period covered 72 days between November 2022 and April 2023 and was followed by a period of issue mitigation.

## Overview of Findings

The auditors remarked that although the scope was large, the number of issues uncovered was low, and that Tor in general adopts "an admirably robust and hardened security posture and sound design decisions." The auditor further said our code was written to a "first-rate standard and conformed to secure coding practices", and that we have adopted highly-advanced and deliberately security focused building processes around Tor Browser because of our reproducible builds, build signing, and more. "All which contribute towards considerable defense-in-depth security posture." They concluded that the components they audited are in a healthy state from a security stand-point.

The audit outlined vulnerabilities, weaknesses and a couple of high-severity issues, alongside a set of recommended fixes and hardening guidance. Overall, the Tor Browser received a positive and satisfactory rating, proving it is "sufficiently robust and hardened against a multitude of common threats and attack vectors."

## Tor Browser & Censorship Circumvention Assessment

Most of the key findings were confined to vulnerable code snippets or did not provide an easy method of exploitation. However, two high-severity issues were discovered that have subsequently been mitigated by the Tor Project following the recommendations from the assessment. The rdsys source code lacked authentication for the resource registration endpoint, previously allowing adversaries to register arbitrary malicious resources for distribution to users. Furthermore, the bridge list returned by rdsys/BridgeDB to the Tor Browser prior to Tor network connectivity wasn't cryptographically signed, providing a possible exploit for malicious actors eavesdropping on the connection or with access to the server providing the bridge list. To mitigate these issues, robust authentication mechanisms for all endpoints were implemented as well as cryptographic means to verify Tor as the distributor, reducing the risk of tampering and unauthorized access respectively.

Despite discovering nineteen issues, which is typical for a project of this scale, the Tor Browser and its ecosystem are considered secure.  Since in some instances the Tor Project relies on third-party libraries, Tor's security is dependent on maintaining and regularly updating those to address any emerging security issues.

Looking ahead, we intend to continue to conduct regular security assessments, and share them with you. These assessments will help maintain and enhance overall security of the Tor ecosystem.

## Tor Browser UI Assessment

Recent changes to the [Tor Browser's user interface (UI) aimed at improving usability](https://blog.torproject.org/new-release-tor-browser-125/) have prompted us to seek another security assessment to ensure that these changes had not inadvertently introduced any security vulnerabilities. While the assessment uncovered some noteworthy findings, none of them posed immediate threats to user privacy and security, but rather offered valuable insights for future development to further strengthen Tor Browser's security: "Of the seven security-related discoveries, three were classified as security vulnerabilities and four were categorized as general weaknesses with lower exploitation potential."

The identified vulnerabilities included the ability for malicious actors to trick users into running JavaScript despite enabling the highest security level, the potential for malicious pages to download an unlimited number of files to the user's download folder, and potential information leaks via custom homepages allowing threat actors to track users across restarts.

All of these vulnerabilities have subsequently been addressed and mitigated by adjusting the data URI protections related to the security levels, and now prompting the user to actively grant permission if they wish to initiate multiple downloads. The report concluded that the recent UI changes did not compromise Tor Browser's overall security. For a comprehensive overview of the findings, you can download that [report here](TTP-02-report.pdf).

We would like to thank Cure53 for performing the audit and their excellent collaboration and communication throughout the process.

