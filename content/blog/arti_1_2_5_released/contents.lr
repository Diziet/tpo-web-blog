title: Arti 1.2.5 is released: onion services development, security fixes
---
author: Diziet
---
pub_date: 2024-06-27
---
categories:

announcements
releases
---
tags:

arti
---
summary:

Arti 1.2.5 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.2.5.

This release contains a number of bugfixes
and a fix to one low-severity
[security issue](https://gitlab.torproject.org/tpo/core/arti/-/issues/1468).
We have improved our testing and made
Tor network path selection more robust.
It continues development on our [planned RPC system].
We have started the initial work on Arti Relay -
the ability to run Arti as a Relay in the Tor Network.

The Arti team has also spent some of our time at
the Tor Project in-person meeting in Lisbon,
where we had numerous productive conversations with
folks who are considering adopting Arti.
Such feedback from users and embedders is very important to us.

For full details on what we've done, and for information about
many smaller and less visible changes as well,
please see the [CHANGELOG].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Alexander Færøy, Gaba, Jim Newsome, juga, pinkforest, and trinity-1686a!

Also, our deep thanks to
[Zcash Community Grants],
the [Bureau of Democracy, Human Rights and Labor],
and our [other sponsors]
for funding the development of Arti!

[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/4ea7e9845bb5d4c395b9559a8ef0af2cd1f9c85b/CHANGELOG.md#arti-125--27-june-2024
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[Bureau of Democracy, Human Rights and Labor]: https://www.state.gov/bureaus-offices/under-secretary-for-civilian-security-democracy-and-human-rights/bureau-of-democracy-human-rights-and-labor/
[other sponsors]: https://www.torproject.org/about/sponsors/
[planned RPC system]: https://gitlab.torproject.org/tpo/core/arti/-/issues/?sort=updated_desc&state=opened&milestone_title=Arti%3A%20RPC%20Support&first_page_size=20
