title: New Alpha Release: Tor Browser 13.0a2 (Android, Windows, macOS, Linux)
---
pub_date: 2023-08-12
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0a2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0a2 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a2/).

This release updates Firefox to 115.1.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-31/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-29/) from Firefox 116.

## Major Changes

This is our second alpha release in the 13.0 series which represents a transition from Firefox 102-esr to Firefox 115-esr. This builds on a year's worth of upstream Firefox changes, so alpha-testers should expect to run into issues. If you find any issues, please report them on our [gitlab](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/new) or on the [Tor Project forum](https://forum.torproject.org/c/feedback/tor-browser-alpha-feedback/6).

We are in the middle of our annual esr transition audit, where we review Mozilla's year's worth of work with an eye for privacy and security issues that would negatively affect Tor Browser users. This will be completed before we transition the 13.0 alpha series to stable. At-risk users should remain on the 102-esr based 12.5 stable series which will continue to receive security updates until 13.0 alpha is promoted to stable.

### Desktop

#### Tor Controller

We have been working on some major refactors and rewrites to the tor daemon controller code in Tor Browser for Desktop. We are unifying and modernizing the competing implementations of various control port interface methods formerly found in the legacy torbutton and tor-launcher components into encapsulated JavaScript modules within the Firefox codebase. This work is part of long-term plan of necessary code-cleanup and lays the groundwork for supporting alternate tor backends besides the legacy tor daemon.

However, all this code-churn *does* open up opportunity for new behaviour due to fixed bugs or due to the introduction of new ones. If you use Tor Browser in a non-standard/non-default configuration (either via Firefox preferences or custom environment variables) please ensure things are working as expected for your configuration with this alpha release!

The areas affected by these changes include:

- configuring Tor Browser to use an external system tor service/daemon
- fetching censorship-circumvention setting using the lyrebird (formerly obfs4proxy) pluggable transport
- any tor functionality that relies on communicating with the tor daemon via the control port (circuit display, onion auth, bridge+network settings, new identity, etc)

#### Tor PoW

This is also the first Tor Browser release including a tor daemon with the new onion service proof-of-work ddos prevention feature. See [Proposal 327](https://gitlab.torproject.org/tpo/core/torspec/-/raw/main/proposals/327-pow-over-intro.txt) for background and the [gitlab issue](https://gitlab.torproject.org/tpo/core/tor/-/issues/40634) regarding the implementation.

### Android

This is our first Android release based on the Firefox 115esr series. Some things are still a bit rough around the edges but, to our knowledge, there are not any known regressions to the browser's core functionality.

## Known Issues

### Windows

To ensure that we are shipping binaries which only contain the functionality we believe they do, we use a reproducible build strategy. The basic idea is that multiple users with build machines running on different networks independently pull down and build the same source code. We then verify that the built binaries we ultimately sign and ship to users are bit for bit identical. This gives us reasonable confidence that our releases have not been compromised and contain only the functionality found in our source code.

During the 13.0a2 release cycle, we have enabled generating debug information for our supported windows platforms to make trouble-shooting windows-specific issues easier. This debug information includes PDB symbols (which map addresses in the binaries to locations in the firefox source code) and generated C/C++ headers. Unfortunately, the header generation is not deterministic, and so different builders will generate different (though semantically equivalent) outputs.

What this means is that, taken as a whole, our builds are not currently matching. However, the mismatched parts only appear in this debug info which is separate from the actual application that is shipped to end-users (this non-matching debug info needs to be actively sought out and is only useful for developers debugging an issue).

This issue is being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41995). It will either be fixed before the 13.0 alpha series transitions to stable later this year, or we will disable this developer feature by default to ensure fully matching builds.

### Android

There are various graphical bugs in the bootstrapping and landing pages in Tor Browser for Android including misaligned text and Firefox branding. The Tor Browser onboarding for first-time users is also missing. These issues (among others) are being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41878), [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41987) and [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41992).

## Full changelog

We would like to thank volunteer contributor FlexFoot for their fix for [tor-browser-build#40615](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40615).
The full changelog since [Tor Browser 13.0a1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.26
  - Updated OpenSSL to 3.0.10
  - Updated tor to 0.4.8.3-rc
  - [Bug tor-browser#41909](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41909): Rebase 13.0 alpha to 115.1.0 esr
- Windows + macOS + Linux
  - Updated Firefox to 115.1.0esr
  - [Bug tor-browser#30556](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/30556): Re-evaluate letterboxing dimension choices
  - [Bug tor-browser#33282](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/33282): Increase the max width of new windows
  - [Bug tor-browser#40982](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40982): Cleanup maps in tor-circuit-display
  - [Bug tor-browser#40983](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40983): Move not UI-related torbutton.js code to modules
  - [Bug tor-browser#41844](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41844): Stop using the control port directly
  - [Bug tor-browser#41907](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41907): The bootstrap is interrupted without any errors if the process becomes ready when already bootstrapping
  - [Bug tor-browser#41922](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41922): Unify the bridge line parsers
  - [Bug tor-browser#41923](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41923): The path normalization results in warnings
  - [Bug tor-browser#41924](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41924): Small refactors for TorProcess
  - [Bug tor-browser#41925](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41925): Remove the torbutton startup process
  - [Bug tor-browser#41926](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41926): Refactor the control port client implementation
  - [Bug tor-browser#41964](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41964): 'emojiAnnotations' not defined in time in connection preferences
- Android
  - Updated GeckoView to 115.1.0esr
  - [Bug tor-browser-build#40919](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40919): Fix nimbus-fml reproducibility of 13.0a2-build1
  - [Bug tor-browser#41928](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41928): Backport Android-specific security fixes from Firefox 116 to ESR 102.14 / 115.1 - based Tor Browser
  - [Bug tor-browser#41972](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41972): Disable Firefox onboarding in 13.0
  - [Bug tor-browser#41997](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41997): Remove all use and reference to com.adjust.sdk.Adjust which now uses AD_ID
- Build System
  - All Platforms
    - Updated Go to 1.20.7
    - [Bug tor-browser-build#31588](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/31588): Be smarter about vendoring for Rust projects
    - [Bug tor-browser-build#40855](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40855): Update toolchains for Mozilla 115
    - [Bug tor-browser-build#40880](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40880): The README doesn't include some dependencies needed for building incrementals
    - [Bug tor-browser-build#40905](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40905): Go vendor archives ignore the nightly version override on testbuilds
    - [Bug tor-browser-build#40908](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40908): Enable the --enable-gpl config flag in tor to bring in PoW functionality
    - [Bug tor-browser-build#40909](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40909): Add dan_b and ma1 to list of taggers in relevant projects
    - [Bug tor-browser-build#40913](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40913): add boklm back to list of taggers in relevant projects
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40615](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40615): Consider adding a readme to the fonts directory
    - [Bug tor-browser-build#40907](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40907): Mar-tools aren't deterministic on 13.0a1
  - Windows
    - [Bug tor-browser-build#31546](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/31546): Create and expose PDB files for Tor Browser debugging on Windows
  - Android
    - [Bug tor-browser-build#40867](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40867): Create a RBM project for the unified Android repository
    - [Bug tor-browser-build#40917](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40917): Remove the uniffi-rs project
    - [Bug tor-browser#41899](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41899): Use LLD for Android
    - [Bug tor-browser-build#40920](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40920): Non-deterministic generation of baseline.profm file in Android apks
