title: Arti 1.2.8 is released: onion services, RPC, and more
---
author: nickm
---
pub_date: 2024-09-30
---
categories:

announcements
releases
---
tags:

arti
---
summary:

Arti 1.2.8 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.2.8.

This release adds numerous new features for onion service key management,
and continues backend development for memory quota support,
Relay support,
and the RPC subsystem.
For full details on what we've done, and for information about
many smaller and less visible changes as well,
please see the [CHANGELOG].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Adam Joseph, Alexander Hansen Færøy, Anonym, Morgan,
Pier Angelo Vendrame, Steven Engler, tidely, and Wesley Aptekar-Cassels.
Also, our welcome to Wesley Aptekar-Cassels as they join the team!

And as always, our deep thanks to
[Zcash Community Grants],
the [Bureau of Democracy, Human Rights and Labor],
and our [other sponsors]
for funding the development of Arti!

[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-128--1-october-2024
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[Bureau of Democracy, Human Rights and Labor]: https://www.state.gov/bureaus-offices/under-secretary-for-civilian-security-democracy-and-human-rights/bureau-of-democracy-human-rights-and-labor/
[other sponsors]: https://www.torproject.org/about/sponsors/
