title: New Alpha Release: Tor Browser 13.5a2
---
pub_date: 2023-11-28
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.5a2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5a2 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5a2/).

This release updates Firefox to 115.5.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-50/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-49/) from Firefox 120.

The full changelog since [Tor Browser 13.5a1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated tor to 0.4.8.9
  - [Bug tor-browser#42153](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42153): Drop dom.enable_resource_timing = false preference
  - [Bug tor-browser#42246](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42246): Migrate tor connection stuff from browser to toolkit
  - [Bug tor-browser#42261](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42261): Update the icon of Startpage search engine
  - [Bug tor-browser#42276](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42276): Rebase Browsers Alpha to 115.5.0esr
  - [Bug tor-browser#42277](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42277): Enable storage.sync to fix broken webextensions
  - [Bug tor-browser#42288](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42288): Allow language spoofing in status messages
  - [Bug tor-browser#42302](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42302): The allowed ports string contains a typo
- Windows + macOS + Linux
  - Updated Firefox to 115.5.0esr
  - [Bug tor-browser#42072](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42072): YEC 2023 Takeover for Desktop Stable
  - [Bug tor-browser#42188](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42188): Donations are asked repeatedly when I click New identity button
  - [Bug tor-browser#42194](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42194): Blank Net Error page on name resolution failure
- Linux
  - [Bug tor-browser#17560](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/17560): Downloaded URLs disk leak on Linux
  - [Bug tor-browser-build#41017](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41017): Disable Nvidia shader cache
- Android
  - Updated GeckoView to 115.5.0esr
  - [Bug tor-browser#41846](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41846): firefox-android esr 115 introduced new nimbus use: they need to be disabled
  - [Bug tor-browser#42074](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42074): YEC 2023 Takeover for Android Stable
  - [Bug tor-browser#42258](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42258): Replace the current boring "fiery android" icon we use for dev with the cool nightly icon
  - [Bug tor-browser#42259](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42259): Remove unused firefox branding from Tor Browser for Android
  - [Bug tor-browser#42260](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42260): Add TBB artifacts to .gitignore
  - [Bug tor-browser#42285](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42285): Update the gitignore to use the correct paths for tor stuff
  - [Bug tor-browser#42287](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42287): Backport security fixes (Android &amp; wontfix) from Firefox 120 to 115.5 - based Tor Browser
- Build System
  - All Platforms
    - Update Go to 1.21.4
    - [Bug tor-browser-build#40884](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40884): Script to automate uploading sha256s and signatures to location signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo expects them to be
    - [Bug tor-browser-build#40970](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40970): Missing symlink create-blog-post.torbrowser -&gt; create-blog-post symlink
    - [Bug tor-browser-build#41006](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41006): Fix typo in finished-signing-clean-linux signer
    - [Bug tor-browser-build#41023](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41023): Update lead.png symlink and blog post template in tools/signing/create-blog-post
    - [Bug rbm#40063](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40063): RBM's chroot fails in Fedora
    - [Bug rbm#40064](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40064): Using exec on project with no git_url/hg_url is causing warning
  - macOS
    - [Bug tor-browser-build#41005](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41005): Unpack macOS bundle to /var/tmp instead of /tmp in rcodesign-notary-submit step
    - [Bug tor-browser-build#41007](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41007): gatekeeper-bundling.sh refers to old .tar.gz archive
    - [Bug tor-browser-build#41014](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41014): Update libdmg-hfsplus to drop the OpenSSL patch
    - [Bug tor-browser-build#41020](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41020): Opening MacOS dmg file is causing a warning, since 13.0
  - Android
    - [Bug tor-browser-build#41024](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41024): Fix android filenames in Release Prep issue templates
