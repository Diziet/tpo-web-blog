title: New Release: Tor Browser 14.0.5
---
pub_date: 2025-02-04
---
author: pierov
---
categories:

applications
releases
---
summary: Tor Browser 14.0.5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 14.0.5 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/14.0.5/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The [full changelog](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-14.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) since Tor Browser 14.0.4 is:

- All Platforms
  - [Bug tor-browser#41065](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41065): navigator.storage "best effort" + "persistent" leak partitionSize/totalSpace entropy
  - [Bug tor-browser#43386](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43386): Extension requests expose Firefox's minor version and custom app name
  - [Bug tor-browser#43447](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43447): Backport stable and legacy: Some .tor.onion sites are not displaying the underlying V3 onion address in alpha
  - [Bug tor-browser#43448](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43448): Rebase Tor Browser release onto 128.7.0esr
  - [Bug tor-browser#43451](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43451): Backport security fixes from Firefox 135
  - [Bug tor-browser-build#41328](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41328): Exclude tor dependencies from LD_LIBRARY_PATH
- Windows + macOS + Linux
  - Updated Firefox to 128.7.0esr
  - [Bug tor-browser#43312](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43312): Download popup panel progress bar overflows
- Linux
  - [Bug tor-browser#43236](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43236): High refresh rate detectable by websites when Wayland (MOZ_ENABLE_WAYLAND=1) is used
  - [Bug tor-browser#43326](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43326): Launching tor-browser on gentoo fails with "version `OPENSSL_3.2.0' not found"
- Android
  - Updated GeckoView to 128.7.0esr
- Build System
  - All Platforms
    - Updated Go to 1.22.11
    - [Bug tor-browser-build#41324](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41324): Improve build signing ergonomics
    - [Bug tor-browser-build#41343](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41343): Add signing step to clean some files such as test artifacts
    - [Bug tor-browser-build#41345](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41345): Go autoupdates in 1.23 so add env var to prevent it in any step that has network access
    - [Bug tor-browser-build#41350](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41350): Increase timeout in rcodesign-notary-submit
    - [Bug tor-browser-build#41357](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41357): keyring/anti-censorship.gpg is in `GPG keybox database version 1` format
