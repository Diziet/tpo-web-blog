title: Arti 1.2.0 is released: onion services development
---
author: gabi
---
pub_date: 2024-03-04
---
categories: announcements
---
summary:

Arti 1.2.0 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.2.0.

In Arti 1.2.0, trying out onion services will hopefully be a
smoother experience.
We have fixed a number of bugs and security issues,
and have made the `onion-service-service` feature non-experimental.

We have begun design work on some of the onion service security features on our roadmap,
such as the memory DoS prevention subsystem,
and the connection bandwidth rate-limiter.
In addition, we have scoped the remaining work
for supporting hidden service client authorization,
which will be implemented in a future release.

This release also fixes a [low severity] security issue:
the relay message handling code was not rejecting empty DATA messages,
which could be used to inject an undetected traffic signal.
This issue is tracked as [TROVE-2024-001].

There are still some rough edges and missing security features,
so we don't (yet) recommend Arti onion services for production use,
or for any purpose that requires privacy.

For instructions on how to run an onion service in Arti,
see our [work-in-progress HOWTO document](https://gitlab.torproject.org/tpo/core/arti/-/blob/arti-v1.2.0/doc/OnionService.md?ref_type=tags).
We hope to make these instructions simpler and better
as our implementation improves.

For full details on what we've done, and for information about
many smaller and less visible changes as well,
please see the [CHANGELOG].

In the next releases, we will focus on implementing
the missing [security features] and on improving stability.

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Alexander Færøy, Jim Newsome, Tobias Stoeckmann, and trinity-1686a.

Also, our deep thanks to [Zcash Community Grants] and our [other sponsors]
for funding the development of Arti!

[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/ef374c925a56be2c308bb78f402eca44d98d377b/CHANGELOG.md#arti-120-4-march-2024
[low severity]: https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/SecurityPolicy#how-will-we-assess-the-severity-of-a-security-issue
[TROVE-2024-001]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1269
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[other sponsors]: https://www.torproject.org/about/sponsors/
[security features]: https://gitlab.torproject.org/tpo/core/arti/-/issues/?label_name%5B%5D=Onion%20Services%3A%20Improved%20Security
[other sponsors]:  https://www.torproject.org/about/sponsors/
