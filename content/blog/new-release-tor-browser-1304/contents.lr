title: New Release: Tor Browser 13.0.4
---
pub_date: 2023-11-21
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 13.0.4 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.4 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.4/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-50/) to Firefox.

This release updates Firefox to 115.5.0esr, tor to 0.4.8.9 and OpenSSL
to 3.0.12. In addition, it also includes some bug fixes (see changelog
for details).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.3](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated OpenSSL to 3.0.12
  - Updated tor to 0.4.8.9
  - [Bug tor-browser#42275](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42275): Rebase Tor Browser Stable to 115.5.0esr
  - [Bug tor-browser#42277](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42277): Enable storage.sync to fix broken webextensions
- Windows + macOS + Linux
  - Updated Firefox to 115.5.0esr
  - [Bug tor-browser#41341](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41341): Fix style and position of "Always Prioritize Onions" wingpanel
  - [Bug tor-browser#42108](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42108): Tor Circuit button not shown if TLS handshake fails
  - [Bug tor-browser#42184](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42184): Setting "Homepage and new windows" ignores "Blank Page" value
  - [Bug tor-browser#42188](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42188): Donations are asked repeatedly when I click New identity button
  - [Bug tor-browser#42194](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42194): Blank Net Error page on name resolution failure
- Windows + macOS
  - [Bug tor-browser#42154](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42154): Empty the clipboard on browser shutdown only if content comes from private browsing windows
- Android
  - Updated GeckoView to 115.5.0esr
  - [Bug tor-browser#42074](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42074): YEC 2023 Takeover for Android Stable
  - [Bug tor-browser#42287](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42287): Backport security fixes (Android &amp; wontfix) from Firefox 120 to 115.5 - based Tor Browser
- Build System
  - All Platforms
    - Update Go to 1.21.4
    - [Bug tor-browser-build#40934](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40934): Remove $bundle_locales from signing scripts now that we're on ALL for everything
    - [Bug tor-browser-build#40982](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40982): Fix logging in tools/signing/do-all-signing
    - [Bug tor-browser-build#40989](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40989): Add .nobackup files to reproducible and disposable directores
    - [Bug tor-browser-build#41006](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41006): Fix typo in finished-signing-clean-linux signer
  - macOS
    - [Bug tor-browser-build#29815](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29815): Sign our macOS bundles on Linux
    - [Bug tor-browser-build#41005](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41005): Unpack macOS bundle to /var/tmp instead of /tmp in rcodesign-notary-submit step
    - [Bug tor-browser-build#41007](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41007): gatekeeper-bundling.sh refers to old .tar.gz archive
    - [Bug tor-browser-build#41014](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41014): Update libdmg-hfsplus to drop the OpenSSL patch
    - [Bug tor-browser-build#41020](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41020): Opening MacOS dmg file is causing a warning, since 13.0
