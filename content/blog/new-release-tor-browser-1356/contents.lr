title: New Release: Tor Browser 13.5.6
---
pub_date: 2024-10-01
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 13.5.6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.6 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.6/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5.4](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated NoScript to 11.4.40
  - [Bug tor-browser#42832](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42832): Download spam prevention should not affect browser extensions
  - [Bug tor-browser#43167](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43167): Rebase Tor Browser Stable onto 115.16.0esr
  - [Bug tor-browser#43173](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43173): Backport security fixes from Firefox 131
- Windows + macOS + Linux
  - Updated Firefox to 115.16.0esr
  - [Bug tor-browser#42737](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42737): Drop the hash check on updates
  - [Bug tor-browser#43098](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43098): YEC 2024 Takeover for Desktop Stable
- Windows + macOS
  - [Bug tor-browser#42747](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42747): Windows 7/8 and macOS 10.12-10.14 Legacy/Maintenance
- Android
  - Updated GeckoView to 115.16.0esr
  - [Bug tor-browser#43099](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43099): YEC 2024 Takeover for Android Stable
