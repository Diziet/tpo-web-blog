title: New Release: Tor Browser 13.0.10
---
pub_date: 2024-02-20
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.10 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.10 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.10/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

This release updates Firefox to 115.8.0esr, OpenSSL to 3.0.13, zlib to 1.3.1, and Snowflake to 2.9.0. It also includes various bug fixes (see changelog for details).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.9](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated OpenSSL to 3.0.13
  - Updated zlib to 1.3.1
  - Updated Snowflake to 2.9.0
  - [Bug tor-browser#42374](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42374): spoof english leaks via numberingSystem: numbers (non-latn) or decimal separator (latn)
  - [Bug tor-browser#42411](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42411): Rebase Tor Browser stable onto 115.8.0esr
  - [Bug tor-browser-build#41079](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41079): Bump version of Snowflake to v2.9.0
- Windows + macOS + Linux
  - Updated Firefox to 115.8.0esr
  - [Bug tor-browser#42338](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42338): Changing circuit programmatically in Tor Browser not working anymore!
- Android
  - Updated GeckoView to 115.8.0esr
  - [Bug tor-browser#42402](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42402): Remove Android YEC strings
  - [Bug tor-browser#42416](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42416): Backport Android security fixes from Firefox 123
- Linux
  - [Bug tor-browser#42293](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42293): Updater is disabled when tor-browser is run by torbrowser-launcher flatpak
- Build System
  - All Platforms
    - Updated Go to 1.20.14 and 1.21.7
    - [Bug tor-browser-build#41067](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41067): Use Capture::Tiny instead of IO::CaptureOutput
    - [Bug rbm#40067](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40067): Use --no-verbose wget option when not running in a terminal
    - [Bug rbm#40068](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40068): Switch from IO::CaptureOutput to Capture::Tiny
    - [Bug rbm#40069](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40069): Make stdout and stderr utf8
    - [Bug rbm#40072](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40072): Move capture_exec to a separate module
