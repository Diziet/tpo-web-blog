title: New Release: Tor Browser 13.5.2
---
pub_date: 2024-08-06
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 13.5.2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.2 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.2/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5.1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated NoScript to 11.4.31
  - [Bug tor-browser#42835](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42835): Filter data transfers containing files
  - [Bug tor-browser#42998](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42998): Rebase Tor Browser stable onto 115.14.0esr
  - [Bug tor-browser#43005](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43005): Backport security fixes from Firefox 129
- Windows + macOS + Linux
  - Updated Firefox to 115.14.0esr
- Android
  - Updated GeckoView to 115.14.0esr
- Build System
  - All Platforms
    - [Bug tor-browser#42470](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42470): Add merge request CI for linting
    - [Bug tor-browser-build#40964](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40964): Create new Tor Browser gpg subkey
    - [Bug tor-browser-build#41168](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41168): deploy_update_responses-$channel.sh should check that it is not reverting an update in an other channel
    - [Bug tor-browser-build#41184](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41184): Update generate blog post script to use new blog header images
    - [Bug tor-browser-build#41190](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41190): Add morgan.gpg to keyrings and list of valid keyrings in firefox+geckoview
    - [Bug tor-browser-build#41191](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41191): Remove richard.gpg from keyrings and list of valid keyrings in firefox+geckoview
    - [Bug tor-browser-build#41204](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41204): NoScript isn't added automatically to Mullvad Browser changelogs
