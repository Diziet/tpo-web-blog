title: New Release: Tor Browser 12.5.1
---
author: richard
---
pub_date: 2023-07-04
---
categories:
applications
releases
---
summary: Tor Browser 12.5.1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:

Tor Browser 12.5.1 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also
from our [distribution directory](https://dist.torproject.org/torbrowser/12.5.1/).

This release updates Firefox to 102.13.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-23/).  There were no Android-specific security updates to backport from the Firefox 115 release..

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 12.5](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.24
  - [Bug tor-browser#41860](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41860): Rebase 12.5 stable to 102.13esr
- Windows + macOS + Linux
  - Updated Firefox to 102.13.0esr
  - [Bug tor-browser#41854](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41854): Download Spam Protection cannot be overridden to allow legitimate downloads
  - [Bug tor-browser#41856](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41856): Onion service authorization prompt's key field does not get focus when clicked
  - [Bug tor-browser#41858](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41858): 'Learn more' link in onboarding links to 12.0 release notes and not 12.5
- Android
  - Updated GeckoView to 102.13.0esr
