title: Privacy-Preserving and Incrementally-Deployable Support for Certificate Transparency in Tor
---
pub_date: 2021-11-30
---
author: guest
---
guest_author: Rasmus Dahlberg, Tobias Pulls, Tom Ritter, and Paul Syverson
---
categories: research 
---
summary: The second issue of PETS 2021 featured a paper that brings Certificate Transparency to Tor. We (Rasmus Dahlberg, Tobias Pulls, Tom Ritter, and Paul Syverson) happen to be the authors and are here to summarize why our work is exciting.
---
body:

## The Why of Certificate Transparency
There are many things that the web could be better at. One part relates to transparent management of TLS certificates. In case you are not familiar with certificates, websites present them to visitors in an attempt to prove their identities. For example, "I'm www.torproject.org and not some imposter".

The problem is that certificates can be issued by many different central authorities. If one of these authorities gets the issuance process wrong, e.g. due to mistakes, coercion, or compromise, there may be a mis-issued certificate for some domain name. A mis-issued certificate can be used by attackers to impersonate websites. This is obviously not great. In the context of Tor it is also easy for an attacker to run an exit to be in a perfect position to perform such attacks. Tor has been shipping the HTTPS Everywhere extension with Tor Browser for some time to stop any attackers at exit relays who try to hijack unencrypted connections. Although this is a valuable protection, it will not stop an attacker with access to a mis-issued certificate.

The goal of Certificate Transparency is to ensure that certificate mis-issuance does not go unnoticed. The idea is that before a browser accepts a certificate as valid, it must be visible in a public Certificate Transparency log. This is excellent, because now anyone can inspect the logs to see for themselves whether there are any mis-issued certificates. If something bad shows up, one can act on that accordingly.

Although the basic idea of Certificate Transparency is simple, the exact instantiation turns out to be less straightforward in the real-world. Should the public logs be parties that you trust blindly? Are there any privacy concerns? What about backwards-compatibility? These are all valid questions that we considered for Tor in particular.

## The How in Tor

Like other browsers that already enforce Certificate Transparency partially, we used trusted logs as a starting point for our addition to Tor Browser. It is not ideal, but much better than what existed before. This reduces the attack surface from hundreds of trusted central authorities that issue certificates to a handful of Certificate Transparency logs--- a significant win for the web!

Next, we showed how to relax these trust assumptions gradually while taking advantage of Tor Browser and the Tor network to preserve privacy. This would be a larger challenge for any browser that cannot leverage an anonymity network with thousands of relays across the globe. For example, to do better than simply having trusted logs available, you need to verify that public logging actually took place. That verification currently requires external interaction with other parties, effectively leaking browsing history to whomever you interact with. In contrast, our incremental designs let Tor relays do that verification for you. Your privacy is preserved by Tor, and aggregate leakages from Tor are reduced via caching.

For more detail we refer the interested reader to our [paper](https://petsymposium.org/2021/files/papers/issue2/popets-2021-0024.pdf) and [presentation](https://www.youtube.com/watch?v=f7yDJOd6g3U).

Next steps include the following:

* Complete partial Certificate Transparency enforcement in Firefox. This would bring Certificate Transparency with trusted logs to both Firefox and Tor Browser.
* Create and implement a torspec proposal that uses Tor relays to increase your confidence that public logging actually happened when using Tor Browser.

## Conclusion

It is exciting to see that Certificate Transparency can be deployed safely in Tor without being restricted to a weak attacker. Our incremental design considers an attacker that controls a fraction of Tor relays, at least one central authority that issues certificates, and all but one Certificate Transparency log. Our full design relaxes these trust assumptions further by allowing the attacker to control all Certificate Transparency logs.
