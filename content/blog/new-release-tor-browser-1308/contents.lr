title: New Release: Tor Browser 13.0.8 (Desktop)
---
pub_date: 2023-12-21
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.8 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.8 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.8/).

This is an emergency desktop-only release which fixes a crash in pluggable transports for Windows 7 users. The most recent Go toolchain update seems to have finally broken Windows 7 support (see [golang/go/#57003](https://github.com/golang/go/issues/57003) for background). We have [downgraded](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/merge_requests/879) our lyrebird, conjure, and webtunnel pluggable transports to the 1.20 series and they should be working once more on older Windows systems. Unfortunately, the snowflake pluggable transport depends on the 1.21 series to work, so it will remain broken on these systems.

We highly encourage Tor Browser users who are still on Windows 7 or 8 to either upgrade their systems to a modern Linux distribution, or at least to a supported version of Windows (10 or higher). Tor Browser will neccessarily have to drop support for Windows 7 and 8 after the ESR 128 transition next year, as Mozilla's minimum Windows operating system requirements is now Windows 10 or newer [as of Firefox 116](https://www.mozilla.org/en-US/firefox/116.0/system-requirements/).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.7](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- Windows
  - [Bug tor-browser-build#41053](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41053): All PT's crash instantly in 13.0.7 
  - [Bug tor-browser#42179](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42179): PTs on Tor Browser 13 do not work with Windows 7
- Linux
  - [Bug tor-browser-build#41050](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41050): Improve the disk leak sanitization on start-$browser
- Build System
  - All Platforms
    - [Bug tor-browser-build#41042](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41042): Add options to include updates in the changelog scripts
    - [Bug tor-browser-build#41043](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41043): Create script to push build requests to Mullvad build servers
