title: New Release: Tails 6.7
---
pub_date: 2024-09-10
---
author: tails
---
categories:

partners
releases
---
summary:

Tails 6.7 is out.
---
body:

## Changes and updates

  * Update _Tor Browser_ to [13.5.3](https://blog.torproject.org/new-release-tor-browser-1353).

  * Update _Thunderbird_ to [115.15.0](https://www.thunderbird.net/en-US/thunderbird/115.15.0esr/releasenotes/).

  * Update _OnionShare_ from 2.2 to 2.6, which includes a feature to create an anonymous chat room.

## Fixed problems

  * Keep the firewall on even during shutdown. ([#20536](https://gitlab.tails.boum.org/tails/tails/-/issues/20536))

  * Stop reporting an error when starting an old Tails USB stick with a system partition of 2.5 GB. ([#20519](https://gitlab.tails.boum.org/tails/tails/-/issues/20519))

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

## Known issues

### Shim SBAT verification error

If you get the following error message when starting your regular Linux
operating system, then it means that your Linux operating system is outdated.

Verifying shim SBAT data failed: Security Policy Violation  
Something has gone seriously wrong: SBAT self-check failed: Security Policy
Violation

  1. Edit your UEFI settings to disable Secure Boot.

With Secure Boot disabled, your regular Linux operating system should start
again.

To learn how to edit the BIOS or UEFI settings, search for the user manual of
the computer on the support website of the manufacturer.

  2. Update your regular Linux operating system.

  3. Try to enable Secure Boot again in your UEFI settings.

If your regular Linux operating system still doesn't start, then disable
Secure Boot again. You can try to enable Secure Boot again in the future.

It might take several months for your Linux distribution to provide updates
before you can enable Secure Boot again.

## Get Tails 6.7

### To upgrade your Tails USB stick and keep your Persistent Storage

  * Automatic upgrades are available from Tails 6.0 or later to 6.7.

  * If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a manual upgrade.

### To install Tails 6.7 on a new USB stick

Follow our installation instructions:

  * Install from Windows

  * Install from macOS

  * Install from Linux

  * Install from Debian or Ubuntu using the command line and GnuPG

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

### To download only

If you don't need installation or upgrade instructions, you can download Tails
6.7 directly:

  * For USB sticks (USB image)

  * For DVDs and virtual machines (ISO image)

## Support and feedback

For support and feedback, visit the [Support
section](https://tails.net/support/) on the Tails website.
