title: New Release: Tor Browser 13.0.5 (Desktop)
---
pub_date: 2023-11-23
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.5 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.5/).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.4](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- Windows + macOS + Linux
  - [Bug tor-browser#42072](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42072): YEC 2023 Takeover for Desktop Stable
- Build System
  - All Platforms
    - [Bug tor-browser-build#40970](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40970): Missing symlink create-blog-post.torbrowser -&gt; create-blog-post symlink
    - [Bug tor-browser-build#41023](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41023): Update lead.png symlink and blog post template in tools/signing/create-blog-post
    - [Bug rbm#40063](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40063): RBM's chroot fails in Fedora
