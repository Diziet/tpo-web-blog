title: New Alpha Release: Tor Browser 13.5a3
---
pub_date: 2023-12-21
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.5a3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5a3 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5a3/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Connect assist comes to Android

This alpha is the first release with Desktop's censorship circumvention feature (connect assist) available on Android! You can try it out for yourself by navigating to the `Settings > Tor Network` and selecting `Enable beta connection features`.

**NOTE**: This feature is very much in development and is still quiet brittle and liable to breakage. Do *not* toggle this option if you value your current Tor Browser for Android Alpha configuration. If this option gets the app into an unusable state, you can get back to the legacy bootstrapping system by clearing the app's storage and cache via the Android system settings. Of course, doing so will also delete any existing configuration options so please do not enable this option if this would be a problem for you.

When this feature ships in 13.5 in late spring/early summer 2024, Android users connecting from censored networks should have the exact same bootstrapping functionality Desktop users currently have. If they are unable to connect to the Tor Network, Tor Browser will query the anti-censorship backend to determine which pluggable transports and bridges they need to use based upon their locale and the current censorship landscape. The browser will then attempt to bootstrap once more with the new settings applied. Our hope is that this will take a lot of the guesswork out of connecting to the Tor Network for our mobile users, and ensure unfettered network access during censorship events.

For now we expect this new bootstrapping UX to be buggy, but we wanted to get it into the hands of users as early as possible so we can iterate and find and fix bugs as early as possible.

### Known Issues

This is still feature is still very much a prototype, so there are a few known issues:

- The 'Enable beta connection features' toggle currently only allows enabling the 'HTML UI'. Unfortunately the 'Native Android UI' option is not quite ready, but it should be available in 13.5a4 in January.
- There is currently a white bar at the bottom of the 'HTML UI' connect assist screen. This 'HTML UI' is actually just the Desktop connect-experience with Android-specific modification to allow us to exercise the backend. It will be replaced with an Android-native frontend before 13.5 stabilises!

## Why was this so hard?

Connect assist has existed on our Desktop platforms [since version 11.5](https://blog.torproject.org/new-release-tor-browser-115/), so why has it taken so long to bring the same functionality to Android?

The fundamental problem is ultimately due to the fact that Tor Browser for Android and Tor Browser for Desktop do not share very much code beyond the low-level Gecko-based browser engine. So while low-level functionality such as fingerprinting, proxy-bypass, and linkability protections are easily shared between the browsers, much of the user-facing functionality people think of when they think of Tor Browser requires reimplementation in the parts which are not shared.

This includes things such as the circuit display, onion service authentication, the security level UI, connect assist and the management of the tor daemon itself. This means that historically, for each of these features, we have needed two parallel implementations: one for Desktop and one for Android (which is why for most of that list we have only one implementation).

The tor daemon management has historically been the most different between our two platforms. On Desktop, we inherited, integrated and encapsulated the tor daemon process and control port communication channels from the `torbutton` and `tor-launcher` XUL Firefox extensions. On Android we have the `tor-android-service` and `tor-onion-proxy-library` Java/Kotlin libraries. The way each of these components interacted with the rest of the browser is very different.

Fortunately, during the Tor Browser 13.0 release cycle we started the effort to [unify our two tor daemon management systems](https://blog.torproject.org/torbutton-retired/) into one.

To summarise, we encapsulated all of the tor daemon management behind a single `TorProvider` [implementation in Tor Browser for Desktop](https://gitlab.torproject.org/tpo/applications/tor-browser/-/blob/tor-browser-115.6.0esr-13.5-1/toolkit/components/tor-launcher/TorProvider.sys.mjs?ref_type=heads), and rewrote all of the browser's calling logic (including connect assist) to use this new interface. So now, instead of having to completely re-implement the entire connect assist system in Java or Kotlin for Android, we instead just need to implement an Android-specific `TorProvider` and direct the existing connect assist backend to use that instead of the Desktop `TorProvider`. From there we 'just' need to [develop the Android native frontend to use it](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41188).

With this coming convergence, the hope is that much of the work related to feature parity between the two platforms becomes a simple(r) matter of implementing the Android frontend and plumbing commands and events to and from the shared backend (rather than re-implementing the entire backend).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5a2](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Tor to 0.4.8.10
  - Updated NoScript to 11.4.29
  - [Bug tor-browser#42042](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42042): view-source:http://ip-address does not work because of HTTPS Only
  - [Bug tor-browser#42308](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42308): Update README for tor browser
  - [Bug tor-browser#42332](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42332): Rebase Tor Browser alpha onto 115.6.0esr
  - [Bug tor-browser#42334](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42334): Keep returning ERROR_ONION_WITH_SELF_SIGNED_CERT only for .onion sites whose cert throws ERROR_UNKNOWN_ISSUER
  - [Bug tor-browser#42335](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42335): Do not localize the order of locales for app lang
  - Bug 42340: TorBridgeChanged notification sends out "[object Object]" as its data. [tor-browser]
- Windows + macOS + Linux
  - Updated Firefox to 115.6.0esr
  - [Bug tor-browser#40856](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40856): Add a default for preferences in TorSettings
  - [Bug tor-browser#42099](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42099): Blind cross-origin requests to .tor.onion domains
  - [Bug tor-browser#42189](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42189): Assertion failure: the value of mPrivateBrowsingId in the loadContext and in the loadInfo are not the same!
  - [Bug tor-browser#42283](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42283): Tor Browser shouldn't ship blockchair by default
  - [Bug tor-browser#42299](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42299): After adding incorrect bridge addres on user cannot go back to the Connection page
  - [Bug tor-browser#42303](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42303): Remove unused "help" button logic in tor dialogs
  - [Bug tor-browser#42319](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42319): Make all the wordmark of the same size
- Windows
  - [Bug tor-browser#42163](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42163): Make the 3rd party DLL blocklist obey portable mode
  - [Bug tor-browser#42179](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42179): PTs on Tor Browser 13 do not work with Windows 7
- Linux
  - [Bug tor-browser-build#41050](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41050): Improve the disk leak sanitization on start-$browser
- Android
  - Updated GeckoView to 115.6.0esr
  - [Bug tor-browser#42248](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42248): Allow GeckoView to launch tor
  - [Bug tor-browser#42249](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42249): Allow GeckoView to launch lyrebird
  - [Bug tor-browser#42250](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42250): Allow Moat.sys.mjs to invoke lyrebird on Android
  - [Bug tor-browser#42301](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42301): Make TorSettings interact with the old Android Settings
  - [Bug tor-browser#42313](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42313): Enable One UI Sans KR as a possible font for Korean (MozBug 1865238)
  - [Bug tor-browser#42323](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42323): Add a checkbox to enable the connect assist experiments on alpha
  - [Bug tor-browser#42324](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42324): Onion Location on Android is ignored
  - [Bug tor-browser#42339](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42339): Backport Android security fixes from Firefox 121 to 115.6 - based Tor Browser
- Build System
  - All Platforms
    - Updated Go to 1.21.5
    - [Bug tor-browser#42331](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42331): tb-dev fetch command is missing repository argument
    - [Bug tor-browser-build#40995](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40995): Use cdn.stagemole.eu instead of cdn.devmole.eu in download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug tor-browser-build#41026](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41026): Do not use ~ when uploading the signed hashes
    - [Bug tor-browser-build#41027](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41027): Remove tb-build-04 and tb-build-05 from tools/signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug tor-browser-build#41036](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41036): Remove go_vendor-lyrebird-nightly makefile target, and rename go_vendor-$project-alpha makefile targets to go_vendor-$project
    - [Bug tor-browser-build#41039](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41039): Update tools/signing/upload-update_responses-to-staticiforme to keep download-*json files from previous release when new release does not include them
    - [Bug tor-browser-build#41042](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41042): Add options to include updates in the changelog scripts
    - [Bug tor-browser-build#41043](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41043): Create script to push build requests to Mullvad build servers
    - [Bug tor-browser-build#41045](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41045): Dump more information about build times on Firefox
    - [Bug tor-browser-build#41048](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41048): Drop the  kcp-go project
  - Windows + macOS + Linux
    - [Bug tor-browser-build#41031](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41031): Add command to unsign .mar files and compare with sha256sums-unsigned-build.txt
    - [Bug tor-browser-build#41056](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41056): Make it possible to use templates in var/torbrowser_incremental_from
    - [Bug tor-browser-build#41057](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41057): make fetch is not fetching mullvad repo
  - Windows + macOS
    - [Bug tor-browser-build#41016](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41016): Switch from bullseye to bookworm for desktop platforms
  - Windows
    - [Bug tor-browser-build#41015](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41015): Enable std::filesystem on libc++ on Windows
    - [Bug tor-browser-build#41030](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41030): Add command to unsign .exe files and compare with sha256sums-unsigned-build.txt
  - macOS
    - [Bug tor-browser-build#40990](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40990): Remove old macos signing scripts
  - Linux
    - [Bug tor-browser-build#41046](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41046): Use the final path for Linux debug symbols
