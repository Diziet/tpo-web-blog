title: New Release: Tor Browser 13.0.1
---
pub_date: 2023-10-25
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.1 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.1/).

This release backports important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-46/) from Firefox 115.4.0esr and

This release updates Firefox to 115.4.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-46/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-45/) from Firefox 119.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - [Bug tor-browser#42185](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42185): Rebase stable browsers on top of 115.4.0esr
  - [Bug tor-browser#42191](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42191): Backport security fixes (Android &amp; wontfix) from Firefox 119 to 115.4 - based Tor Browser
  - [Bug tor-browser-build#40975](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40975): libstdc++.so.6 is included twice in tor-browser
- Windows + macOS + Linux
  - Updated Firefox to 115.4.0esr
  - [Bug tor-browser#42182](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42182): Default Search Engine Does Not Persist Through Shift to New Identity
- Android
  - Updated GeckoView to 115.4.0esr
- Build System
  - All Platforms
    - Updated Go to 1.21.3
    - [Bug tor-browser-build#40976](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40976): Update download-unsigned-sha256sums-gpg-signatures-from-people-tpo to fetch from tb-build-02 and tb-build-03
    - [Bug rbm#40062](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40062): Copy input directories to containers recursively
  - Windows + Linux
    - [Bug tor-browser-build#40991](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40991): Fix creation of downloads-windows-x86_64.json and downloads-linux-x86_64.json
  - Windows
    - [Bug tor-browser-build#40984](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40984): The PDBs for .exe are not included
  - Linux
    - [Bug tor-browser-build#40979](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40979): Add redirects from old Linux bundle filename to the new one
