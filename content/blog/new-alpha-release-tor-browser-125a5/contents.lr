title: New Alpha Release: Tor Browser 12.5a5 (Android, Windows, macOS, Linux)
---
pub_date: 2023-04-21
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.5a5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.5a5 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.5a5/).

This release updates Firefox 102.10.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-14/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-13/) from Firefox 112.

We use this opportunity to update various other components of Tor Browser as well:

- NoScript 11.4.21

## Build-Signing Infrastructure Updates

We are in the process of updating our build signing infrastructure, and unfortunately are unable to ship code-signed 12.5a5 installers for Windows systems currently. Therefore we will not be providing full Window installers for this release. However, automatic build-to-build upgrades should continue to work as expected. Everything should be back to normal for the 12.5a6 release next month!

## Full changelog

The full changelog since [Tor Browser 12.5a4](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.21
  - Updated Go to 1.19.8
  - [Bug tor-browser-build#40833](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40833): base-browser nightly is using the default channel instead of nightly
  - [Bug tor-browser#41687](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41687): Rebase Tor Browser Alpha to 102.10.0esr
  - [Bug tor-browser#41689](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41689): Remove startup.homepage_override_url from Base Browser
  - [Bug tor-browser#41704](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41704): Immediately return on remoteSettings.pollChanges
- Windows + macOS + Linux
  - Updated Firefox to 102.10esr
  - [Bug mullvad-browser#165](https://gitlab.torproject.org/tpo/applications/mullvad-browser/-/issues/165): Fix maximization warning x button and preference
  - [Bug tor-browser#40501](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40501): High CPU load after tor exits unexpectedly
  - [Bug tor-browser#40701](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40701): Improve security warning when downloading a file
  - [Bug tor-browser#40788](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40788): Tor Browser 11.0.4-11.0.6 phoning home
  - [Bug tor-browser-build#40811](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40811): Make testing the updater easier
  - [Bug tor-browser-build#40831](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40831): Fix update URL for base-browser nightly
  - [Bug tor-browser#40958](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40958): The number of relays displayed for an onion site can be misleading
  - [Bug tor-browser#41038](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41038): Update "Click to Copy" button label in circuit display
  - [Bug tor-browser#41109](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41109): "New circuit..." button gets cut-off when onion name wraps
  - [Bug tor-browser#41350](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41350): Move the implementation of Bug 19273 out of Torbutton
  - [Bug tor-browser#41521](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41521): Improve localization notes
  - [Bug tor-browser#41533](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41533): Page Info window for view-source:http://...onion addresses says Connection Not Encrypted
  - [Bug tor-browser#41600](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41600): Some users have difficulty finding the circuit display
  - [Bug tor-browser#41617](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41617): Improve the UX of the built-in bridges dialog
  - [Bug tor-browser#41668](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41668): Move part of the updater patches to base browser
  - [Bug tor-browser#41686](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41686): Move the 'Bug 11641: Disable remoting by default' commit from base-browser to tor-browser
  - [Bug tor-browser#41695](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41695): Port warning on maximized windows without letterboxing from torbutton
  - [Bug tor-browser#41699](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41699): Tighten up the tor onion alias regular expression
  - [Bug tor-browser#41701](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41701): Reporting an extension does not work
  - [Bug tor-browser#41702](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41702): The connection pill needs to be centered vertically
  - [Bug tor-browser#41709](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41709): sendCommand should not try to send a command forever
  - [Bug tor-browser#41711](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41711): Race condition when opening a new window in New Identity
  - [Bug tor-browser#41713](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41713): “Remove All Bridges” button only appears after hitting “Show All Bridges"
  - [Bug tor-browser#41714](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41714): “Show Fewer Bridges” button missing from refactored remove all bridges UI
  - [Bug tor-browser#41719](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41719): Update title and button strings in the new circuit display to sentence case
  - [Bug tor-browser#41722](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41722): Regression: window maximization warning cannot be closed by the X button
  - [Bug tor-browser#41725](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41725): Stray connectionPane.xhtml patch
- Windows
  - [Bug tor-browser#41459](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41459): WebRTC fails to build under mingw
  - [Bug tor-browser#41678](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41678): WebRTC build fix patches incorrectly defining pid_t
  - [Bug tor-browser#41683](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41683): Disable the network process on Windows
- Linux
  - [Bug tor-browser-build#40830](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40830): The fontconfig directory is missing in Base Browser
  - [Bug tor-browser#41163](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41163): Many bundled fonts are blocked in Ubuntu/Fedora because of RFP
- Android
  - Updated GeckoView to 102.10esr
  - [Bug tor-browser#41724](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41724): Backport Android-specific security fixes from Firefox 112 to ESR 102.10-based Tor Browser
- Build System
  - All Platforms
    - [Bug tor-browser-build#40828](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40828): Use http://archive.debian.org/debian-archive/ for jessie
    - [Bug tor-browser-build#40837](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40837): Rebase mullvad-browser build changes onto main
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40823](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40823): Update appname_* variables in projects/release/update_responses_config.yml
    - [Bug tor-browser-build#40826](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40826): Correctly set appname_marfile for basebrowser in tools/signing/nightly/update-responses-base-config.yml
    - [Bug tor-browser-build#40827](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40827): MAR generation uses (mostly) hard-coded MAR update channel
    - [Bug tor-browser#41730](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41730): Bridge lines in tools/torbrowser/bridges.js out of date
  - Windows
    - [Bug tor-browser-build#40822](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40822): The Tor Browser installer doesn't run with mandatory ASLR on (0xc000007b)
  - macOS
    - [Bug tor-browser-build#40824](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40824): dmg2mar script using hardcoded project names for paths
    - [Bug tor-browser-build#40844](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40844): DMG reproducibility problem on 12.0.5
  - Linux
    - [Bug tor-browser-build#40835](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40835): Update faketime URLs in projects/container-image/config
  - Android
    - [Bug tor-browser#41684](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41684): Android improvements for local dev builds
