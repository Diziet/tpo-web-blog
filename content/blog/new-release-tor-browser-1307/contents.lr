title: New Release: Tor Browser 13.0.7
---
pub_date: 2023-12-19
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.7 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.7 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.7/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

This release updates Firefox to 115.6.0esr, tor to 0.4.8.10 and NoScript to 11.4.29. In addition, it also includes some bug fixes (see changelog for details).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.6](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated tor to 0.4.8.10
  - Updated NoScript to 11.4.29
  - [Bug tor-browser#42042](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42042): view-source:http://ip-address does not work because of HTTPS Only
  - [Bug tor-browser#42261](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42261): Update the icon of Startpage search engine
  - [Bug tor-browser#42330](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42330): Rebase stable browsers to 115.6.0esr
  - [Bug tor-browser#42334](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42334): Keep returning ERROR_ONION_WITH_SELF_SIGNED_CERT only for .onion sites whose cert throws ERROR_UNKNOWN_ISSUER
- Windows + macOS + Linux
  - Updated Firefox to 115.6.0esr
  - [Bug tor-browser#42283](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42283): Tor Browser shouldn't ship blockchair by default
- Android
  - Updated GekcoView to 115.6.0esr
  - [Bug tor-browser#42285](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42285): Update the gitignore to use the correct paths for tor stuff
  - [Bug tor-browser#42339](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42339): Backport Android security fixes from Firefox 121 to 115.6 - based Tor Browser
- Build System
  - All Platforms
    - Update Go to 1.21.5
    - [Bug tor-browser-build#40884](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40884): Script to automate uploading sha256s and signatures to location signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo expects them to be
    - [Bug tor-browser-build#41026](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41026): Do not use ~ when uploading the signed hashes
    - [Bug tor-browser-build#41036](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41036): Remove go_vendor-lyrebird-nightly makefile target, and rename go_vendor-$project-alpha makefile targets to go_vendor-$project
    - [Bug tor-browser-build#41039](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41039): Update tools/signing/upload-update_responses-to-staticiforme to keep download-*json files from previous release when new release does not include them
  - macOS
    - [Bug tor-browser-build#40990](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40990): Remove old macos signing scripts
