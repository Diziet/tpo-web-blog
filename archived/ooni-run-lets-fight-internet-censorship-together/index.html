<!doctype html>
<html>
<head>
    <title>OONI Run: Let&#39;s fight internet censorship together! | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="OONI Run: Let&#39;s fight internet censorship together! | Tor Project">
    <meta property="og:description" content="Today, the Tor Project’s Open Observatory of Network Interference (OONI) team released OONI Run, a website linked to an exciting new OONI Probe mobile app feature that enables you to engage your friends (and the world) to run censorship measurement tests and monitor the blocking of your website around the world.">
    <meta property="og:image" content="https://blog.torproject.org/ooni-run-lets-fight-internet-censorship-together/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/ooni-run-lets-fight-internet-censorship-together/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        OONI Run: Let&#39;s fight internet censorship together!
      </h1>
    <p class="meta">by agrabeli | September 27, 2017</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Today, the Tor Project’s <a href="https://ooni.torproject.org/">Open Observatory of Network Interference (OONI)</a> team released <a href="https://run.ooni.io/"><strong>OONI Run</strong></a>, a website linked to an exciting new OONI Probe mobile app feature that enables you to:</p>
<ul>
<li>Engage your friends (and the world) to run censorship measurement tests</li>
<li>Monitor the blocking of <em>your</em> website around the world</li>
</ul>
<p>OONI Run includes a variety of <a href="https://github.com/TheTorProject/ooni-probe">OONI Probe software tests</a> designed to:</p>
<ul>
<li>Test the <a href="https://ooni.torproject.org/nettest/web-connectivity/">blocking of websites</a></li>
<li>Find <a href="https://ooni.torproject.org/nettest/http-invalid-request-line/">middleboxes</a></li>
<li>Measure the <a href="https://ooni.torproject.org/nettest/ndt/">speed and performance</a> of networks</li>
<li>Measure <a href="https://ooni.torproject.org/nettest/dash/">video streaming performance</a></li>
</ul>
<p>By choosing the tests that interest you and the sites you want to test, generate a link via <a href="https://run.ooni.io/">OONI Run</a> and share it. Alternatively, embed the widget code to monitor the accessibility of your site. The global OONI Probe community can then respond!</p>
<h2>Engage your friends</h2>
<p>With <a href="https://run.ooni.io/">OONI Run</a>, you can <strong>choose which websites you want to test</strong> for censorship. Simply select “Web Connectivity” and add as many URLs as you like. </p>
<p><img alt="OONI Run: Web Connectivity" height="428" src="/static/images/blog/inline-images/web-connectivity.png" width="696" /></p>
<p>Then click “Generate” to generate a link that you can share with your friends. </p>
<p><img alt="OONI Run: Links and code" height="343" src="/static/images/blog/inline-images/share.png" width="655" /></p>
<p>If they already have OONI Probe installed, the link will open their mobile app and automatically start testing the sites of your choice! Otherwise, it will encourage them to <a href="https://ooni.torproject.org/install/">install OONI Probe</a> first.</p>
<h2>Engage the world</h2>
<p><strong>OONI Run can help build a global network for rapid response to emergent censorship events.</strong> By tweeting a generated link, you can encourage the world to run the tests (and to test the sites) you’ve chosen.</p>
<p>If you suspect that a site might be censored during political events, add that site to <a href="https://run.ooni.io/">OONI Run</a>, generate a link, and share it with people in that country. If you heard rumors of censorship, engage people in that country to collect data that can serve as evidence.  </p>
<h2>Monitor your site</h2>
<p>Perhaps your site has been blocked by some governments and you're unsure how the censorship is implemented or whether it’s blocked across all networks. Or perhaps you’re just interested in having data that can serve as evidence that your site was intentionally censored.</p>
<p>You can monitor the accessibility of your site through the following simple steps:</p>
<ul>
<li>Select “Web Connectivity” on <a href="https://run.ooni.io/">OONI Run</a></li>
<li>Add your site</li>
<li>Click “Generate”</li>
<li>Embed the provided widget code and encourage the global OONI Probe community to test your site!</li>
</ul>
<p>You can embed a nice widget on your website by including the widget source like this:</p>
<pre>
&lt;script src=' https://cdn.jsdelivr.net/npm/ooni-run/dist/widgets.js'&gt;&lt;/script&gt;
</pre><p>You can then add buttons to your website by adding this element:</p>
<pre>
 &lt;a href='https://run.ooni.io/nettest?tn=web_connectivity&amp;ta=%7B%22urls%22%3A%5B%22https%3A%2F%2Fwww.wikipedia.org%22%5D%7D&amp;mv=1.2.0' class='ooni-run-button'&gt;Run OONI!&lt;/a&gt;
</pre><p>This is how the button will look like on your website:</p>
<p><img alt="OONI button" src="/static/images/blog/inline-images/ooni-button.png" /></p>
<p>Otherwise, if you would like a bigger widget, you can add (and tailor) the following element to your website:</p>
<pre>
      &lt;div data-link='https://run.ooni.io/nettest?tn=web_connectivity&amp;ta=%7B%22urls%22%3A%5B%22https%3A%2F%2Fwikipedia.org%22%5D%7D&amp;mv=1.2.0' class='ooni-run-banner'&gt;
      Test Wikipedia
    &lt;/div&gt;
</pre><p>It can look like this:</p>
<p><img alt="Wikipedia widget" height="366" src="/static/images/blog/inline-images/wikipedia-widget.png" width="473" /></p>
<p>Going forward, more <a href="https://ooni.torproject.org/nettest/">OONI Probe tests</a> will be added to <a href="https://run.ooni.io/">OONI Run</a>. Together, we can coordinate on fighting internet censorship around the world.</p>
<p> </p>
<p><em>Note: OONI Run is now available on Android and within 24-48 hours (depending on Apple) it will be available on iOS too.</em></p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-271491"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271491" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Seth Schoen (not verified)</span> said:</p>
      <p class="date-time">September 27, 2017</p>
    </div>
    <a href="#comment-271491">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271491" class="permalink" rel="bookmark">I&#039;ve been curious about why…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've been curious about why censors haven't treated OONI tests differently from other network traffic (either completely blocking all traffic from OONI nodes, completely <em>unblocking</em> all traffic from OONI nodes, returning random blocks rather from the normal ones, or trying to physically locate them and shut them down). It seems like this tool will make it much easier to reliably locate OONI nodes without doing much effort—a censor can create a site called this-is-an-ooni-node.censor.gov.tk or whatever, submit it to OONI Run, and then see who connects to that site. Is there little or no adversarial response against the OONI project itself so far?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271492"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271492" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  agrabeli
  </article>
    <div class="comment-header">
      <p class="comment__submitted">agrabeli said:</p>
      <p class="date-time">September 27, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271491" class="permalink" rel="bookmark">I&#039;ve been curious about why…</a> by <span>Seth Schoen (not verified)</span></p>
    <a href="#comment-271492">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271492" class="permalink" rel="bookmark">Hi Seth,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Seth,</p>
<p>I'm pasting Arturo's response to your questions (posted on the tor-talk mailing list) here:</p>
<p>"When you click on a link with OONI Run the user is presented with a page that shows them the list of URLs (if the test is web_connectivity) that they are about to test.</p>
<p>If a malicious URL is included in such list, the user will be able to avoid running it by simply clicking the X button.</p>
<p>The run.ooni.io links are also defined as a custom URI inside of both apps, so if the app is installed, the user will actually never be connecting to ooni.io servers.</p>
<p>That said, something to keep in mind, is that OONI Probe is not a privacy tool, but rather a tool for investigations and as such poses some risks (as we explain inside of our informed consent procedure).</p>
<p>We are not aware of any OONI Probe users having gotten into trouble for using our tool, but we prefer to air always on the safe side be sure that they understand very well the speculative risks that they could run into."</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-271495"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271495" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>+ (not verified)</span> said:</p>
      <p class="date-time">September 28, 2017</p>
    </div>
    <a href="#comment-271495">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271495" class="permalink" rel="bookmark">And offcourse…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And offcourse</p>
<p>Fight censorship by using the new Torbrowser 7.0.6 and Tails 3.2 that can be downloaded since september 25th but are not mentioned anywhere on (blog.)torproject.org nor tails.boum.org.<br />
Why is that? What is going on? Are there 7.0.7 and 3.2.1 versions upcoming??</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271497"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271497" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hip_pu (not verified)</span> said:</p>
      <p class="date-time">September 28, 2017</p>
    </div>
    <a href="#comment-271497">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271497" class="permalink" rel="bookmark">i had trouble with hkps but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i had trouble with hkps but i do not know if it is considered as a censorship or a survey / blocked / down site /maintenance : hkps is used for uploading:downloading gnupg keys.</p>
<p>i disagree about the usage (not the goal) of your tool ooni.</p>
<p>- i do not care that is censored ; i take care about the authenticity_integrity of the sites i visit,<br />
- i do not care of wikipedia {wiki(s) are obsoletes and give a false value of a definition, explanations.} ; i surf on the sites  which i need for setting , configuring , communicating.<br />
- does ooni work for onion sites ?<br />
- during politic events TorBlog was censored and sometimes bad behavior occurred :<br />
a) different tab had the same relays,<br />
b) https broken, noscrips broken,<br />
c) vpn shut down before or after the tor check,<br />
d) going to a site then going back (pushing the arrow on the bar) showed the alert 'you are not connected at the tor network' (checked before).<br />
e) is a site hacked has the same sens that a site censored or with an unstable/slow connection ?<br />
f) bridge did not work,<br />
g) dns did not respond,</p>
<p>i think that these attacks do not depend on politic event but are planned like a test or an update of a secret agreement.<br />
i suppose it was a state sponsored attack with the help of famous enterprises/firms (and their clients) which the work is to struggle against the censorship/d.o.s.</p>
<p>i noticed that the doc on the web are outdated and there are a lot of fake sites so google (startpage &amp; duckduckgo run it) is a partner of the censorship.</p>
<p>OONI is more for admins of website and for cellphone, tablet than desktop users.</p>
<p>OONI should have some default sites like hkp/hkps e.g.</p>
<p>i should not use it because it is not an anonymous tool.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonX (not verified)</span> said:</p>
      <p class="date-time">September 30, 2017</p>
    </div>
    <a href="#comment-271595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271595" class="permalink" rel="bookmark">Related resource:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Related resource:</p>
<p>To check the authenticity of a secure web connection your device is making (any starting with httpS://) use this page by tech guru Steve Gibson:</p>
<p><a href="https://www.grc.com/fingerprints.htm" rel="nofollow">https://www.grc.com/fingerprints.htm</a></p>
<p>His server will go out and grab the unique identity (SHA1 hash)  of the security certificate of the web site you want to check.  If the number Gibson's site shows is the same as the number your browser shows then you are connected to the genuine website, not a fake one.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271780"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271780" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ParrotSec (not verified)</span> said:</p>
      <p class="date-time">October 06, 2017</p>
    </div>
    <a href="#comment-271780">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271780" class="permalink" rel="bookmark">Tell me whether I can use…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tell me whether I can use Parrot O.S. for anonymous surfing on the network when it's properly configured ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273430"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273430" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hello (not verified)</span> said:</p>
      <p class="date-time">January 16, 2018</p>
    </div>
    <a href="#comment-273430">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273430" class="permalink" rel="bookmark">hi…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi<br />
please create OONI for windows<br />
thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
