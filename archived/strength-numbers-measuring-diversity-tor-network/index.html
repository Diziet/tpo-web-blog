<!doctype html>
<html>
<head>
    <title>Strength in Numbers: Measuring Diversity in the Tor Network | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Strength in Numbers: Measuring Diversity in the Tor Network | Tor Project">
    <meta property="og:description" content="Just as in nature, greater diversity among the members of the Tor network&#39;s ecosystem increases the sustainability of the Tor network, the biggest benefit of which is to ensure users are more secure and better protected from traffic correlation attacks.">
    <meta property="og:image" content="https://blog.torproject.org/strength-numbers-measuring-diversity-tor-network/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/strength-numbers-measuring-diversity-tor-network/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Strength in Numbers: Measuring Diversity in the Tor Network
      </h1>
    <p class="meta">by irl | December 11, 2018</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company and we are all safer and stronger when we work together. <a href="https://torproject.org/donate/donate-sin-md">Please contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<p>The Tor network is an ecosystem made up of thousands of volunteer-run relays distributed across the world that enable users to make private connections to services on the internet. Just as in nature, greater diversity among the members of the Tor network's ecosystem increases the sustainability of the Tor network, the biggest benefit of which is to ensure users are more secure and better protected from traffic correlation attacks.</p>
<p>At the <a href="https://metrics.torproject.org/">Tor Metrics portal</a>, we archive historical data about the Tor ecosystem, collect data from the public Tor network and related services, and assist in developing novel approaches to safe, privacy preserving data collection, including stats on network diversity.</p>
<p>With additional funding and support, Tor Metrics could provide even more important information about Tor network diversity than it already does. We created the graphs in this post through a one-off analysis for the purpose of demonstrating how valuable this information can be when visualized and easily accessible.</p>
<p>Diversity can be accomplished through a variety of operating systems used by different relays (e.g., Linux or FreeBSD), the computer architecture (e.g., PC or RaspberryPi), the geographical location (e.g., the country or continent), or the hosting provider. Although we only have a snapshot view of the <a href="https://metrics.torproject.org/">latest statistics for provider or country</a>, we do track the <a href="https://metrics.torproject.org/platforms.html?start=2007-09-04">number of relays by their operating system over time</a>.</p>
<p>In the past, the Tor Metrics portal did provide a graph for country statistics over time, but maintaining the graph <a href="https://bugs.torproject.org/8127">became too expensive</a>, due to development time, storage costs, computing resources, and ongoing maintenance.</p>
<p>If we had this back on our Metrics portal, we could always have these statistics for the top 5 countries running relays:</p>
<p><img alt="Relays by countries " src="/static/images/blog/inline-images/relaycountries-relays-2018-11-30_0.png" class="align-center" /></p>
<p>We can see from this graph that since 2015, Germany has surpassed the United States with a greater number of relays. We also see that from 2014, France has seen a larger rise in relays compared to the Netherlands and Russia. This graph looks at absolute numbers of individual relays but, in fact, this is only part of the picture. When you use Tor, a path through the network is chosen for you by your client. Each relay in this path is chosen by its "consensus weight fraction” which is based on the bandwidth of each relay so that load is balanced across all the available relays. Low bandwidth relays have a lower probability of being chosen while high bandwidth relays have a higher probability. In the graph above, we treat all relays as equal, so a more accurate view would be based on the "consensus weight fraction" per country:</p>
<p><img alt="Consensus weight fraction per country " src="/static/images/blog/inline-images/relaycountries-consweight-2018-11-30_0.png" class="align-center" /></p>
<p>This graph looks at relative values as opposed to absolute values, so we no longer see the overall upward trends, but instead see over time how likely it is that a relay from each country will be chosen for use by clients. Looking at the most recent values, France now appears as more likely to be chosen than the United States even though we saw in the previous graph that the United States has more relays. This is because individual relays in the United States cannot handle as much traffic as relays in France. Even though the United States has over twice as many relays as the Netherlands, it has roughly the same total relay capacity and probability of selection. We also see that Russia drops out of the top 5 and is replaced by Sweden.</p>
<p>We can also go a step further. Different relays are better for different positions in a circuit. If a relay is quite stable, it is a good choice for the first relay, or "Guard," which <a href="https://support.torproject.org/en-US/tbb/tbb-2/">will remain the first relay</a> in a circuit over a long period. If a relay allows exit connections to the internet, then it is better used for those connections as the final relay, or "Exit," as opposed to using its bandwidth to only relay traffic to another.</p>
<p><img alt="Guard and Exit weight fraction by country " src="/static/images/blog/inline-images/relaycountries-guardexit-2018-11-30_0.png" class="align-center" /></p>
<p>Interestingly, there is little difference between the probability that a relay will be selected for the "Guard" position vs. the "Exit" position for the latest values in the top 5 countries, but this has not always been the case.</p>
<p>Ongoing robust network measurement is essential in order to monitor the health and diversity of the Tor network, respond to censorship events, to adapt Tor Browser and other apps to respond to changing network conditions, and to validate changes that are made to the software that runs the Tor network.</p>
<p>As stated above, though, we had to remove this function from the Tor Metrics portal when it became prohibitively expensive. We would like to add these graphs back to enable easy tracking of geographical diversity for relays. If you would like to help bringing back these graphs, please consider <a href="https://torproject.org/donate/donate-sin-md">donating to help support this work</a>. <strong>A recurring donation would help us to keep these visualizations and other Tor Metrics services running for years to come. </strong></p>
<p>Analyzing a live anonymity system must be performed with great care so that our users' privacy is not put at risk, and our numbers are valuable to the researchers, relay operators, and developers who help us keep the network strong.</p>
<p><a href="https://torproject.org/donate/donate-sin-md"><img alt="donate-button" src="/static/images/blog/inline-images/tor-donate-button_7.png" class="align-center" /></a></p>
<p>Plus, every donation now through the end of 2018 will be matched by Mozilla. There is strength in numbers. <a href="https://torproject.org/donate/donate-sin-md">Join us</a>.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/metrics">
          metrics
        </a>
      </li><li>
        <a href="../category/relays">
          relays
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-278873"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278873" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonym (not verified)</span> said:</p>
      <p class="date-time">December 11, 2018</p>
    </div>
    <a href="#comment-278873">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278873" class="permalink" rel="bookmark">very good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>very good</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278886"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278886" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Random donor (not verified)</span> said:</p>
      <p class="date-time">December 11, 2018</p>
    </div>
    <a href="#comment-278886">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278886" class="permalink" rel="bookmark">Fantastic progress and an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fantastic progress and an encouraging report!</p>
<p>I am one of the users who first tried to draw attention to the need for diversity many years ago so it has been very rewarding to see the effort people have been putting into this very important project!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278961"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278961" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2018</p>
    </div>
    <a href="#comment-278961">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278961" class="permalink" rel="bookmark">A day in the life of top…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A day in the life of top-European Torcircuits</p>
<p>Germany, Germany, Germany<br />
Germany, France, Netherlands.<br />
Germany, Netherlands, France<br />
Germany, France, France<br />
Germany, Netherlands, Netherlands<br />
Germany, Germany, Germany<br />
Germany, France, France<br />
Germany, Netherlands, Netherlands<br />
Germany, Germany, Germany</p>
<p>And maybe, maybe after renewing circuits more times, maybe finally another country in sight.<br />
On certain websites (webmail services) you will almost not succeed, it is just stuck to this nabor friends country combinations.</p>
<p>Just coincidences, over and over again or an example of good 'nabors' with a very good international tornetwork cooperation?<br />
Looking at these network combinations everyday it is quite common (like the example above) to have all three in a row, or a row with 2 of this three parties.<br />
Or, when renewing network exchanging place for the exit and middle node.</p>
<p>Would it be possible to influence the redirections to another node?<br />
If so, then we have a secret service surveillance network within tornetwork based on nodes that are owned by these services.<br />
That is interesting metadata (for them).</p>
<p>Wouldn't it a better idea to spread these connections.<br />
Divide them over continents, never using an entry node in the country you are in, no nabor country connections.<br />
Borders are disappearing, at least in the digital world, big chance that you are communicating over 3 nodes in a row that are owned by cooperating owners.</p>
<p>Network related question, ....<br />
anyone else got hacked last saturday when visiting lemonde.fr website with torbrowser?</p>
<p>Browser crash after enabling javascripts and torbrowser left broken behind, startup tornetwork function not working anymore, totally kaputt.<br />
Never happened in many years. Looked like maybe a javascript, and svg based attack?<br />
Only a final complete new reinstall of the browser was a solution, visiting that .fr website without javascripts enabled was possible finally. </p>
<p>Chapeau to the hacking guys.<br />
Next time, try to chapeau to Rousseau again!<br />
I was just trying to read the news, a quite important basic (but apparently disappearing) right.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279031"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279031" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2018</p>
    </div>
    <a href="#comment-279031">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279031" class="permalink" rel="bookmark">Everyone is talking about …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Everyone is talking about "diversity" yet the vast majority of nodes are from a few EU countries and the US which closely cooperate and can easily share surveillance data.</p>
<p>I think this is a serious problem.<br />
What is the reason for the complete lack of nodes from Asia, South America or even Africa?<br />
Many countries there have very robust IT infrastructure yet to me it feels as if the diversity of the tor network has actually decreased over the years as capacity has increased.<br />
Let's face it, most nodes are not run by your Average Joe individual but by organizations nowadays which is not astonishing given the bandwidh they provide. Still I'd say something similar should be possible in different regions at least to some degree. You hold Tor summits around the globe so maybe focusing on gaining relay operators outside of EU / US would not be the worst idea?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279149"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279149" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">December 20, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279031" class="permalink" rel="bookmark">Everyone is talking about …</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279149">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279149" class="permalink" rel="bookmark">One issue is that the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>One issue is that the servers that measure the bandwidth a relay can provide are located in Europe and the US and so relays running further away tend to be assigned a lower consensus weight and used less. Another issue is the lack of existing communities in some locations to help get relays running.</p>
<p>We are currently looking at improving the bandwidth measurement system by replacing the existing software with new software and we do hold relay operator meetups co-located with events that a few Tor people are attending, such as our recent meeting in Mexico.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
