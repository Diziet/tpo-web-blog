<!doctype html>
<html>
<head>
    <title>Some notes on progress with IOCP and Libevent | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Some notes on progress with IOCP and Libevent | Tor Project">
    <meta property="og:description" content="Hi! I recently wrote up a status report for the progress we&#39;re making on hacking Libevent, and I...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/some-notes-progress-iocp-and-libevent/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Some notes on progress with IOCP and Libevent
      </h1>
    <p class="meta">by nickm | March 1, 2009</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Hi!  I recently wrote up a status report for the progress we're making on hacking Libevent, and I thought I'd post it here too.</p>

<p><b>BACKGROUND</b></p>

<p>Tor currently uses Libevent for its high-performance networking calls.  Libevent is a software library originally written by Niels Provos (then of UMichigan, now of Google), and now co-developed by Niels Provos and the Tor Project's Nick Mathewson.  Its purpose is to provide consistent fast interfaces to various operating systems' mutually incompatible fast networking facilities.  Libevent gives applications two basic interfaces to these networking layers: a low-level interface where the application is notified when an operation (like a network read or write) is <i>ready to begin</i>, and a higher-level interface where Libevent itself manages network operations and the application is notified when the network operations are <i>completed</i>.</p>

<p>Existing versions of Libevent have good performance everywhere but on Windows.  This is because, while all other remotely common server operating systems provide a fast networking facility suitable for Libevent's low-level interface, Windows's fast ("IOCP") networking calls are only suitable for building a compatible version of Libevent's high-level interface.  Additionally, Libevent has not even used these fast Windows calls for its high-level interface, because of certain deficiencies in Libevent's existing implementation (such as lack of full Windows compatibility in all its submodules, lack of thread-safe data structures, and inefficient data structures).</p>

<p>For Windows, using IOCP is not just a performance requirement, but a stability one.  Many non-server versions of Windows require all network IO buffers to use "non-paged RAM" (memory that doesn't get swapped to disk), and limit the total amount of RAM that can be used for non-paged storage to an uncomfortably low amount.</p>

<p>So to make Libevent fast and stable on Windows, the tasks are:</p>

<p>  1) Make Libevent's overall design flexible enough to support an IOCP-based backend for its high-level interface.</p>

<p>  2) Write the IOCP-based backend for Windows.</p>

<p>Additionally, Tor today only uses Libevent's low-level interface because of missing features in the high-level interface, such as support for SSL-based connections and rate-limiting.  Also, while the speed-performance of the high-level interface has been fine, it has used unacceptably large amounts of RAM in existing versions of Libevent.  So to make Tor take advantage of Libevent's high-level networking capabilities, the tasks are:</p>

<p>  3) Revise Libevent's high-level interface to support the features Tor needs.</p>

<p>  4) Revise Tor's own networking layer to use Libevent's high-level interface.</p>

<p>Finally, Windows's IOCP performs best when it is run in an aggressively multi-threaded environment, where any one of a pool of worker threads might be notified about any operation's completion on any socket.  Moving to a model of this kind is out-of-scope for this report, though Libevent and Tor both plan to move in this direction in the longer term.</p>

<p>STATUS</p>

<p>Items 1 and 3 are mostly done: Nick has re-written large portions of Libevent's underlying high-level ("bufferevents") interface in order to support multiple backends; to run safely in multiple threads; to allow for arbitrary data filters (including SSL and compression) and connection restraints (including rate limiting); and to provide acceptable RAM usage by use of improved data structures.  These improvements and others are slated for inclusion in Libevent 2.0, which will represent the largest revision in Libevent since its beginning.</p>

<p>Item 2 is begun and underway.  We expect to have the first version of a Windows IOCP-based implementation of the high-level Libevent interfaces completed by the end of March; perhaps sooner.  After this point, we'll need to spend a while load-testing the implementation to ensure it behaves correctly and quickly.  Additionally, our conference outreach has found us a volunteer domain expert in this area who has offered to help us out with high-load testing.</p>

<p>Item 4 will start once the Libevent interfaces have been tested enough that we are relatively confident of their stability.  We will have two options at this point.  First, we could make future versions of Tor require Libevent 2.0 everywhere, and replace Tor's buffered network IO code with calls to Libevent's.  Alternatively, we could build Tor to use the low-level Libevent interface if built against an older Libevent, but use the high-level interface if built against a more recent one.  The former approach would be easier to build, but might create unacceptable dependencies for some package bundlers depending on when the various free operating systems begin to ship Libevent 2.  We will work with them to identify which approach is best for our users.</p>

    </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-872"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-872" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2009</p>
    </div>
    <a href="#comment-872">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-872" class="permalink" rel="bookmark">Vidalia bug</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Vidalia is failing to clear proxy address.</p>
<p>In Settings, entering an address for a proxy and a port number seems to be permanent. Even if the proxy details are cleared, starting Tor always results in an error message and the Log shows<br />
Mar 26 01:45:43.562 [Notice] Tor v0.2.0.34 (r18423). This is experimental software. Do not rely on it for strong anonymity. (Running on Windows XP Service Pack 3 [workstation] {terminal services, single user})<br />
Mar 26 01:45:45.812 [Warning] Couldn't look up "proxy"<br />
Mar 26 01:45:45.812 [Warning] Failed to parse/validate config: HttpsProxy failed to parse or resolve. Please fix.<br />
Mar 26 01:45:45.812 [Error] Reading config failed--see warnings above.</p>
<p>Any help, short of re-installing Vidalia ? Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-873"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-873" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-872" class="permalink" rel="bookmark">Vidalia bug</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-873">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-873" class="permalink" rel="bookmark">So you are trying to use a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So you are trying to use a proxy to reach the Tor network to get around a firewall or something like that?</p>
<p>I'm not sure what sort of firewall it is, but telling your client to only connect to entry nodes on ports 80 or 443 or using bridges might be a better option. If one of those sounds good, say something, and me or someone else should be able to provide more detailed instructions.</p>
<p>Anyway, have you tried manually editing the configuration file?</p>
<p>Sorry, I'm not sure where it is stored on Windows, being a *nix user myself, but there should be a torrc and maybe a special Vidalia configuration file around somewhere.</p>
<p>Not that you should have to edit the configuration file to disable something you enabled graphically, but until someone figures out why Vidalia isn't working as expected and releases an update, that's probably your best option.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2676"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2676" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 28, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-873" class="permalink" rel="bookmark">So you are trying to use a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2676">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2676" class="permalink" rel="bookmark">Any updates?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any updates on the progress of this? I noticed that</p>
<p><a href="http://code.google.com/p/spserver/downloads/list" rel="nofollow">http://code.google.com/p/spserver/downloads/list</a></p>
<p>has an implementation of IOCP + libevent available for the 1.4 and 2.0 branches.  Are you aware of this implementation?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-3107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Thanks (not verified)</span> said:</p>
      <p class="date-time">November 13, 2009</p>
    </div>
    <a href="#comment-3107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3107" class="permalink" rel="bookmark">http://blog.torproject.org/blog/some-notes-progress-iocp-and-lib</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you, you answered the question I have been searching for which was whether or not to place keywords when blog commenting. <a href="http://www.hayda.net" rel="nofollow">mirc</a> . <a href="http://www.hayda.net" rel="nofollow">chat</a> . <a href="http://www.hayda.net/" rel="nofollow">http://www.hayda.net/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-6941"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-6941" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 18, 2010</p>
    </div>
    <a href="#comment-6941">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-6941" class="permalink" rel="bookmark">I must add here that to run</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I must add here that to run safely in multiple threads; to allow for arbitrary data filters (including SSL and compression) and connection restraints (including rate limiting); and to provide acceptable RAM usage by use of improved data structures.<br />
<a href="http://pencilsharpenerelectric.com/" rel="nofollow">http://pencilsharpenerelectric.com/</a><br />
<a href="http://causesof-divorce.com/" rel="nofollow">http://causesof-divorce.com/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
