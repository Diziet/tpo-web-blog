<!doctype html>
<html>
<head>
    <title>Tor in 2021 | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Tor in 2021 | Tor Project">
    <meta property="og:description" content="This year has been difficult for all of us. Although challenging, we have managed to reorganize in order to meet the goals we originally set for 2020, and now, it’s time to look forward to 2021.">
    <meta property="og:image" content="https://blog.torproject.org/tor-in-2021/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/tor-in-2021/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Tor in 2021
      </h1>
    <p class="meta">by isabela | December 10, 2020</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>This year has been difficult for all of us. As individuals, we’ve had to adapt to the new normal of COVID-19, and as an organization, the Tor Project also had to adapt to our “new normal” after we made the difficult decision to let go of one third of our organization. Although challenging, we have managed to reorganize in order to meet the goals we originally set for 2020, and now, it’s time to look forward to 2021.</p>
<p><strong>Continuing User-First Development</strong></p>
<p>Since I joined the Tor Project back in 2015 as a Project Manager, my primary goal has been to incorporate a user-centric development method, and create a way for us to test our tools with users and use their feedback to guide our work. This was not an easy task; in 2015, we didn’t even have a UX team, and since we don’t collect invasive user data like much of the tech industry, we needed to build a way to directly engage with our users face-to-face in order to do this work. For the past three years, <a href="https://www.youtube.com/watch?v=pKh3Sn7Y6Ww">every Tor Browser release has featured usability improvements</a> guided by this program. In 2021, we will continue to work on improving our tools based on feedback from users, and we hope to be focusing on Tor Launcher by incorporating it to Tor Browser and <a href="https://www.youtube.com/watch?v=NeTVSLQsThk">improving the usability for censored users</a> when bypassing censorship and connecting to Tor.</p>
<p><strong>Improving Network Performance and Network Health</strong></p>
<p>An important part of improving the usability of Tor’s tools is improving the performance of the Tor network. We started the foundation for work to <a href="https://www.youtube.com/watch?v=zQDbfHSjbnI">improve network performance in 2020 and we are looking forward to continuing it in 2021</a>. Some of the experiments we conducted this year showed an amazing improvement in network speed, and we hope that this work will address one of the major complaints we hear, that “Tor is slow.”</p>
<p><a href="https://www.youtube.com/watch?v=LwKGfG0bYTI">Network health is a related area of work that requires our attention</a>. In 2019, we founded a Network Health team, but because of the layoffs in 2020, we had to direct our staff capacity to other parts of the organization. We already have items on our roadmap to ensure the continuation of this work for 2021, and we hope to add a dedicated person to support our relay operators community. Relay operators are foundational to the existence of Tor, and ensuring a good relationship with them is fundamental for the health of the network and community. We will also continue our work to <a href="https://www.usenix.org/conference/usenixsecurity20/presentation/komlo">implement the Walking Onions</a>, which removes the need for clients to download the full network consensus when connecting to Tor, making it possible to add more relays without degrading performance or security on the network.</p>
<p><strong>Expanding Mobile Support</strong></p>
<p>Another area we have invested in the past is to bring the ‘mobile first’ mentality to our development, we invested in making a <a href="https://blog.torproject.org/new-release-tor-browser-1003">Tor Browser for Android</a> and have <a href="https://gitlab.torproject.org/legacy/trac/-/issues/23684">done some improvements in our network to help mobile apps developers to incorporate support for Tor to their apps</a>. In 2021 we hope to create the foundation work for some major changes that will contribute to our ‘mobile first’ approach. We are investigating different needs users have while using Tor on Desktop in comparison with using Tor on Mobile. We are investing in making sure that our anti-censorship tools work well in a mobile environment.</p>
<p><strong>Developing Arti</strong></p>
<p>If you watched the recent <a href="https://www.youtube.com/watch?v=Xpg775CJkaY">State of the Onion, you probably saw Nick Mathewson talk about Arti</a>, which stands for “A Rust Tor Implementation,” in which we are using the Rust programming language as it is much safer and productive to work with. Our goal is to create a smaller, safer Tor client that will be easier to embed in applications, especially mobile apps. Arti will ensure more collaboration and be easier to maintain, ensuring Tor’s longevity. We hope that by the end of 2021, we have something we can recommend for secure use and start testing. Eventually Arti could also become something that relays can use. Arti could eventually have a more robust API for developers to help them integrate Tor to their applications. We also hope to continue adding support for IPv6 and other types of support, such as UDP traffic, to Tor.</p>
<p><strong>Investigating Tokens</strong></p>
<p>In 2020, we spent time <a href="https://www.youtube.com/watch?v=542d7FooV84">exploring the possibility of implementing tokens</a> to solve a variety of issues, <a href="https://blog.torproject.org/stop-the-onion-denial">including DDoS attacks against onion services</a> and requesting bridges. In 2021, we hope to continue this exploration and we hope to design a token system for Tor as well as the initial implementation of this system in some basic use cases. This is a lot of work, and we still need to ensure funds to support this process, but we see how tokens can be useful for many use cases in the future, so we will do our best to ensure we can pursue this in 2021 and move it into a more concrete area of work.</p>
<p><strong>Working to Unblock Tor</strong></p>
<p>In 2020, we ran a <a href="https://blog.torproject.org/more-onions-end-of-campaign">campaign called #MoreOnionsPorFavor</a> to increase visibility to onion service technology and its adoption. We received great engagement from a wide range of different sites and services, including blogs, news agencies, law firms, human rights NGOs, email and VPN providers, cryptocurrency sites,and other products that integrated .onion addresses and Onion-Location as a privacy and security feature for their users. In 2021, we want to continue this type of outreach work and initiate a campaign to call on services that are blocking connections from the Tor network to unblock Tor and stop punishing privacy and anonymity. We plan to invest in educational materials that address some of the reasons these services might have to be blocking Tor, offer alternative solutions, and we hope that this campaign will improve our users' experience as they browse the web.</p>
<p><strong>Welcome 2021!</strong></p>
<p>Like I said at the beginning of this blog post, 2020 has been a challenging year for everyone, and it was not different for Tor. </p>
<p>As the year ends, we can look back and see how we made the best out of all these challenges. The reorganization of the Tor Project opened new channels of communication within the organization that weren’t in place before. We started a weekly All Hands meeting to sync as an organization, and we have introduced <a href="https://lists.torproject.org/pipermail/tor-project/2020-November/003014.html">Demo Days</a> that are open for our community to share the projects we’re all working on. I would say that our culture has evolved within this process. Sometimes when an organization is faced with the challenges like we faced this year, organizational culture can suffer, and I am proud to say that was not the case for us at Tor. </p>
<p>I am very thankful for the team we have and for the community that is always around us and supporting us. Our plans for 2021 demonstrate how ambitious we are in making a better Tor that’s accessible to everyone and how dedicated we are to fulfilling our mission. And in times like these--where an internet connection has become crucial for workers, educators, health care, human rights defenders, journalists to do their work--we understand how important it is that everybody knows that Tor is here for them to use and that it will continue to improve. </p>
<p>If you want to support the projects I’ve outlined above, and all the work Tor does to bring privacy online to millions of people, <strong>please <a href="https://torproject.org/donate/donate-usetor-bp-ny">make a donation</a></strong>. Your contributions make an impact on whether or not we can complete these plans, and every donation, no matter the size, makes a difference.</p>
<p><a href="https://torproject.org/donate/donate-usetor-bp-ny"><img alt="tor donate button" src="/static/images/blog/inline-images/tor-donate-green-button2020_2.png" class="align-center" /></a></p>
<p>Thank you for always supporting us, I wish everyone at Tor and who are part of our community a great 2021. </p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/fundraising">
          fundraising
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Comments are closed.</p>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
