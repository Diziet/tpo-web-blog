<!doctype html>
<html>
<head>
    <title>Support the Tor Network: Donate to Exit Node Providers | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Support the Tor Network: Donate to Exit Node Providers | Tor Project">
    <meta property="og:description" content="The Tor network is run by volunteers, and for the most part is entirely independent of the...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/support-tor-network-donate-exit-node-providers/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Support the Tor Network: Donate to Exit Node Providers
      </h1>
    <p class="meta">by mikeperry | October 10, 2011</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>The Tor network is run by volunteers, and for the most part is entirely independent of the software development effort led by The Tor Project, Inc. While The Tor Project, Inc is a 501(c)3 non-profit that is <a href="https://www.torproject.org/donate/donate.html.en" rel="nofollow">happy to take donations</a> to create more and better software, up until recently there was no way for you to fund deployment of more relays to improve network capacity and performance, aside from running those relays yourself.</p>

<p>We're happy to announce that both <a href="https://www.noisebridge.net/wiki/Tor" rel="nofollow">Noisebridge</a> and <a href="https://www.torservers.net" rel="nofollow">TorServers.net</a> are now able to take donations directly for the purpose of running high capacity Tor exit nodes.</p>

<p>Noisebridge is a US 501(c)3 non-profit, which means that for US citizens, <a href="http://tor.noisebridge.net/" rel="nofollow">donations</a> are tax deductible. Torservers.net is a German non-profit organization whose <a href="https://www.torservers.net/donate.html" rel="nofollow">donations</a> are tax deductible for German citizens (and <a href="http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:62007J0318:EN:HTML" rel="nofollow">also potentially</a> for citizens of other EU member states).</p>

<p>What are the pluses and minuses of donating as opposed to running your own relay? Glad you asked!</p>

<p>While it is relatively easy and risk-free to run a middle relay or a bridge, running an exit can be tough. You have to seek out a <a href="https://trac.torproject.org/projects/tor/wiki/doc/TheOnionRouter/GoodBadISPs" rel="nofollow">friendly ISP</a>, explain Tor to them, and then navigate a <a href="https://blog.torproject.org/blog/tips-running-exit-node-minimal-harassment" rel="nofollow">laundry list of Internet bureaucracies</a> to ensure that when abuse happens, the burden of <a href="https://trac.torproject.org/projects/tor/wiki/TheOnionRouter/TorAbuseTemplates" rel="nofollow">answering complaints</a> falls upon you and not your ISP.</p>

<p>These barriers are all made easier the larger your budget is. On top of this, like most things, bandwidth is cheaper in bulk. But not just <a href="http://www.costco.com/Browse/Product.aspx?prodid=11487214" rel="nofollow">Costco cheaper</a>: exponential-growth cheaper, all the way up into the gigabit range (and perhaps beyond, but no one has run a Tor node on anything faster).</p>

<p>At these scales, large exit nodes can pay as little as $1/mo per dedicated megabit/second. Sometimes less. This means that <a href="https://www.paypal.com/subscriptions/business=treasurer%40noisebridge.net&amp;item_name=Noisebridge%20Tor%20Node%20Project&amp;cy_code=USD&amp;no_note=1&amp;no_shipping=1&amp;a3=30&amp;p3=1&amp;t3=M&amp;src=1" rel="nofollow">adding $30/mo</a> to the hosting budget of a large exit node can buy almost 40 times more full-duplex dedicated bandwidth than a similarly priced business upgrade to your home ADSL line would buy, and about 50 times more bandwidth than Amazon EC2 instances at the entry-level price of $0.08 per half-duplex gigabyte, not counting CPU costs. (<a href="https://www.torproject.org/docs/bridges" rel="nofollow">Bridge</a> economics in terms of IP address space availability might still favor Amazon EC2, but that is a different discussion).</p>

<p>The downside to donation is that network centralization can lead to a more fragile and a more observable network. If these nodes fail, the network will definitely feel the <a href="https://metrics.torproject.org/performance.html" rel="nofollow">performance</a> <a href="https://lists.torproject.org/pipermail/tor-talk/2011-September/021493.html" rel="nofollow">loss</a>. In terms of observability, fewer nodes also means that fewer points of surveillance are required to deanonymize users (though <a href="http://archives.seul.org/or/dev/Sep-2008/msg00016.html" rel="nofollow">some argue</a> that more users will make such surveillance less reliable, no one has yet rigorously quantified that result against actual attacks).</p>

<p>Therefore, if you are able to run a high capacity relay or exit yourself (or have access to cheap/free/<a href="https://gitweb.torproject.org/tor.git/blob/master:/contrib/linux-tor-prio.sh" rel="nofollow">unused</a> bandwidth at your work/school), running your own relay is definitely preferred. If you are part of the Tor community and want to accept donations, we'd love to add you to our <a href="https://www.torproject.org/docs/faq#RelayDonations" rel="nofollow">recommended donor list</a>. Please join us on the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays" rel="nofollow">tor-relays mailing list</a> to discuss <a href="https://github.com/noisetor/" rel="nofollow">node configuration</a> and <a href="https://www.torservers.net/wiki/guides" rel="nofollow">setup</a>.</p>

<p>However, if configuring and maintaining a high capacity relay is not for you, donating a portion of the monthly hosting budgets of either of these organizations is an excellent way to support anonymity, privacy, and censorship circumvention for very large numbers of people.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/advocacy">
          advocacy
        </a>
      </li><li>
        <a href="../category/network">
          network
        </a>
      </li><li>
        <a href="../category/relays">
          relays
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-12070"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12070" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2011</p>
    </div>
    <a href="#comment-12070">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12070" class="permalink" rel="bookmark">oops.geolocation ability is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>oops.geolocation ability is a real danger for privacy.please inform us if you can tell more detailed</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12073"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12073" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2011</p>
    </div>
    <a href="#comment-12073">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12073" class="permalink" rel="bookmark">The need for this is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The need for this is symptomatic of a flawed policy towards Relays. The Tor Project evidently views all types of communications as equal, i.e., instant messaging = internet relay chat = web sites. The fact is different types of communications yield different types of content. The intellectual level of instant messaging is not much higher than a 10th grader in high school. In light of this reality, does instant messaging deserve the same priority as web sites? The point is by pushing Relays to offer all services the Tor Project is causing Relays to pull back and play it safe by offering only the dubious service of being a Non-Exit Relay.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12076"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12076" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
    <a href="#comment-12076">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12076" class="permalink" rel="bookmark">How about a central fund for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How about a central fund for exit node operators where all the related donations are collected?</p>
<p>After a period an exit node operator may claim his share which is a percentage of the fund calculated by the observed uptime and bandwidth of his exit node.<br />
Unclaimed shares are going back into the fund after some time to be paid out in the following period to the exit node operators who claim their share.</p>
<p>This fund would be an incentive for everyone to operate an exit node, to provide more bandwidth and to upgrade the hardware.</p>
<p>There should be a public log of which nodes have never claimed an outpayment so that these can assert their non-commercial status.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12079"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12079" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
    <a href="#comment-12079">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12079" class="permalink" rel="bookmark">I see that Noisebridge is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I see that Noisebridge is running a 0.2.3.x version of Tor.  In their Twitter feed they explain that this it to help Iranians who cannot get Internet access.  What's wrong with v0.2.2.33, the current stable version?</p>
<p>Is it really smart to run a development version of Tor on multiple high-speed relays?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12080"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12080" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
    <a href="#comment-12080">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12080" class="permalink" rel="bookmark">What does the Tor network</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What does the Tor network need most at this point to improve anonymity and performance, more nodes or more bandwidth?</p>
<p>I understand that both are desirable, but which would be the most beneficial? Would one be better off creating a number of lower bandwidth nodes or a single high-bandwidth node?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12083"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12083" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12080" class="permalink" rel="bookmark">What does the Tor network</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12083">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12083" class="permalink" rel="bookmark">We don&#039;t have a proper</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We don't have a proper metric for comparing the anonymity of one Tor network topology to another, which we need in order to properly answer your question. Creating such a metric is actually an open research problem: <a href="https://blog.torproject.org/blog/research-problem-measuring-safety-tor-network" rel="nofollow">https://blog.torproject.org/blog/research-problem-measuring-safety-tor-…</a></p>
<p>The best we can say right now is that it depends on the expenses and the node location distribution. When we're talking about the ability to provide 100X or even just 10X more bandwidth for the same cost, I think creating a single high-bandwidth node wins. The more people use Tor, the better everything will get: from larger anonymity sets to companies and websites taking our userbase seriously instead of just blocking us.</p>
<p>It also doesn't make a whole lot of sense to fire up a bunch of smaller nodes if they are all at the same ISP anyway. For them to really provide more anonymity than a single node, you want them to be at different points on the Internet topology.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12081"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12081" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
    <a href="#comment-12081">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12081" class="permalink" rel="bookmark">&gt; The Tor Project evidently</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; The Tor Project evidently views all types of communications as equal...</p>
<p>I see your point, but there's no good answer to your point about relative "value" of data.  Yeah, most IM is stupid. It is equally true that a huge number of IM or e-mail messages can be transferred for the "cost" of a single hi-res graphic.  Does that suggest a punitive view of graphics traffic?  </p>
<p>The only thing I feel confident about re Tor data traffic is that bittorrent is too much of a hog to let it dominate the network.  If Tor latencies were much better I'd worry about streaming video even more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12084"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12084" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
    <a href="#comment-12084">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12084" class="permalink" rel="bookmark">Hello. I downloaded Tor, or</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello. I downloaded Tor, or thought I did. I found it listed in DuckDuckGo. I can't get it to start. It wants an executable. I'm not a programmer. I'm just someone tired of google and facebook trash tracking me. </p>
<p>Is there a way to get help?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12085" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2011</p>
    </div>
    <a href="#comment-12085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12085" class="permalink" rel="bookmark">On the surface this seems</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On the surface this seems like a positive development, but like other posters, I'm quite skeptical of major (I think it's fair to so characterize this) changes in either Tor Project philosophical direction or Tor network environment. Just to play Devil's Advocate, is it not possible (or even likely) that a very high concentratioh of Tor exit nodes and/or traffic within a single net block (or contiguously-owned blocks) will attract a disproportionate amount of adversarial attention? Frankly, if I had to make a choice between a highly-anonymous service that could only deliver text and limited graphics at a reasonable speed, and a less-anonymous service that could let me view and upload/download streaming, high-fidelity media, I would choose the former service without hesitation. Peering variants still work for media access.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 13, 2011</p>
    </div>
    <a href="#comment-12112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12112" class="permalink" rel="bookmark">How about setting a goal of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How about setting a goal of convincing 25% or 50% of the current non-exit Relays to offer an exit for web sites only? At very low risk to the Relays and at no cost to the Tor Project, the speed of access for Tor users should increase considerably.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12145"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12145" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 16, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12112" class="permalink" rel="bookmark">How about setting a goal of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12145">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12145" class="permalink" rel="bookmark">Ports 80 and 443 are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ports 80 and 443 are unfortunately not 'very low risk'. People do all sorts of crazy stuff on the web, and other people are used to complaining about it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12159"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12159" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-12159">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12159" class="permalink" rel="bookmark">Terabytes of data have gone</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Terabytes of data have gone through my Exit Relay and I have received one bland e-mail from my ISP. I suppose what constitutes "very low risk" is in the eye of the beholder. Believe it or not, flying is safer than driving.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-12192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12192" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 19, 2011</p>
    </div>
    <a href="#comment-12192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12192" class="permalink" rel="bookmark">I am setting up a web site</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am setting up a web site soon, with fully-featured forum, blog, etc. Is there an option from the groups above to support an exit-enclave for my soon-to-be-web site? Is that even possible and/or a smart thing to do? </p>
<p>I am still looking for a web site host, off-shore to the US is important, as is ease of setup. I was thinking about setting up a web site on the same ISP used by one of those non-profit exit node groups listed above. Would that make it possible from them to run an exit enclave? </p>
<p>I would also prefer to offer hidden service access to my web site, even though the web site is on the 'normal' Internet. Would the be an option from the groups above? Would that even be a smart or useful thing to do?</p>
<p>And of course, my web site and all content is legal and non-boutique in all but the most draconian 3rd world countries (and China ;)).</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12227"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12227" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 21, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12192" class="permalink" rel="bookmark">I am setting up a web site</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12227">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12227" class="permalink" rel="bookmark">At a guess: This is probably</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>At a guess: This is probably not a good idea. The hidden service case doesn't help you very much beyond what the exit enclave provides, because it is possible to intersect your uptime with the public relay list (which takes several months, but does work eventually).</p>
<p>You might gain a lot of traffic analysis resistance due to traffic volumes for the exit enclave case, but in both cases, it seems too easy for either party to incur extra risk of equipment seizure. In my opinion, this factor alone makes it not worth explicitly associating the two entities.</p>
<p>However, the ISPs in use by high-capacity exit nodes are definitely great places to host. They obviously respect Internet freedom and are not likely to scare easy in the face of baseless legal threats.</p>
<p>Note that pairing with a high capacity middle relay for your enclave should not change either party's risk profile, as far as I know. So that remains as another option.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
