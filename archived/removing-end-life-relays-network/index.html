<!doctype html>
<html>
<head>
    <title>Removing End-Of-Life Relays from the Network | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Removing End-Of-Life Relays from the Network | Tor Project">
    <meta property="og:description" content="Today, the Tor network is composed of more than 6000 relays. These relays are running Tor...">
    <meta property="og:image" content="https://blog.torproject.org/removing-end-life-relays-network/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/removing-end-life-relays-network/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Removing End-Of-Life Relays from the Network
      </h1>
    <p class="meta">by dgoulet | October 8, 2019</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Today, the Tor network is composed of more than 6000 relays. These relays are running Tor software versions that go all the way back to the 0.2.4.x series, which was released on December 10th 2013. Other cutting edge relays are running our latest code in nightly builds and alpha releases. These relay versions represent roughly 5 years of Tor development. There are a total of 85 different Tor versions, from alpha to stable, in use by relays today.</p>
<p>Currently the Tor Network Team maintains <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/CoreTorReleases#Listofreleases">5 Tor version series</a>:</p>
<ul>
<li>0.2.9.x (<a href="https://en.wikipedia.org/wiki/Long-term_support">LTS</a>)</li>
<li>0.3.5.x (LTS)</li>
<li>0.4.0.x</li>
<li>0.4.1.x</li>
<li>0.4.2.x (Stable on Dec 15th, 2019)</li>
</ul>
<p>Maintaining these versions means that the Network Team intends to fix major stability issues, security vulnerabilities, and portability regressions. We may also fix smaller bugs that significantly impact user experience.</p>
<h2>Impact of EOL Relays</h2>
<p>Unfortunately, End-Of-Life relays have some negative impacts on the network. Any relay in the network that runs an obsolete version puts network stability and security at risk. Outdated relays make it harder for us to roll out important fixes. And they can also make it harder to roll out some new features.</p>
<p>One example is the Denial of Service defenses that we rolled out at the start of 2018 as an emergency reaction to a large scale attack on the network. Unfortunately, that defense is only available for relays running supported versions.</p>
<p>Tor's circuit padding defense is in a similar position. Tor 0.4.1.x introduced circuit padding, to make it harder for network observers to recognize client onion service requests. The feature only works for circuits that have a 0.4.1.x (or later) relay as their middle hop.</p>
<p>Because of a <a href="https://trac.torproject.org/projects/tor/ticket/27841">bug</a> in the 0.3.2.x series, some out of date relays are badly affecting HSv3 reachability.  These relays cause higher latency than normal for users. And they add overall network load, from all the Tor clients retrying failed HSv3 connections.</p>
<h2>Removal of EOL relays</h2>
<p>Because of the impacts of keeping unmaintained relays on the network, we've decided to remove End-Of-Life relays from the network.</p>
<p>In the past weeks, we've taken steps to contact every relay operator with a valid ContactInfo field to ask them to upgrade to the latest stable release. The Tor relay community <a href="https://lists.torproject.org/pipermail/tor-relays/2019-September/017711.html">was informed</a> via the tor-relays mailing list on September 3rd 2019 of this upcoming change.</p>
<p>The End-Of-Life relays in the network currently make up just over 12% of the total bandwidth, or around 750 relays. Out of these, only 62 are Exit relays accounting for only 1.68% of the total Exit traffic. We expect a minor impact on the size of the network, and a <a href="https://metrics.torproject.org/bandwidth.html">small drop in the Metrics graph</a>.</p>
<p>Specifically, today we're starting the process of having the 9 directory authorities refuse End-Of-Life relays. The affected series are:</p>
<ul>
<li>0.2.8.x (278)  [w: 0.55%]</li>
<li>0.3.0.x (20)    [w: 0.04%]</li>
<li>0.3.1.x (34)    [w: 0.15%]</li>
<li>0.3.2.x (136)  [w: 1.37%]</li>
<li>0.3.3.x (81)    [w: 0.84%]</li>
<li>0.3.4.x (205)  [w: 9.12%]</li>
</ul>
<p>We expect our next Tor stable release (around November 2019) will contain a software change that will <a href="https://trac.torproject.org/projects/tor/ticket/31549">reject End-Of-Life relays by default</a>. Until then, we will reject around 800 obsolete relays using their fingerprints.</p>
<p>Obsolete bridges won't be rejected yet; they will be rejected later in 2019, when we deploy the Tor software change.</p>
<h2>Upgrading Your End-of-Life Relay</h2>
<p>If you see this line in your logs, then your relay was part of the EOL list:</p>
<blockquote><p>Fingerprint is marked rejected -- if you think this is a mistake please set a valid email address in ContactInfo and send an email to <a href="mailto:bad-relays@lists.torproject.org">bad-relays@lists.torproject.org</a> mentioning your fingerprint(s)?</p>
</blockquote>
<p>In order to re-join the network, please upgrade to a still supported version and ideally our latest stable (currently 0.4.1.6).</p>
<p>After you upgrade your relay, there are two ways to get back on the Tor network:</p>
<ul>
<li>If you want to keep your relay keys, email the bad relay list, and ask them to stop rejecting your relay fingerprint.</li>
<li>Or change your relay keys:
<ol>
<li>Go to your Tor data directory (DataDirectory).</li>
<li>Then delete the <strong>keys/ </strong>directory (Linux: "rm -rf keys/")</li>
<li>Start Tor again.</li>
</ol>
</li>
</ul>
<p>You can find <a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide#PlatformspecificInstructions">Tor packages for your distro / OS here</a>.</p>
<p>Debian and Ubuntu users: Please note that those instructions use our<a href="https://support.torproject.org/apt/tor-deb-repo/"> deb.torproject.org repository</a>. Using our package repositories (or <a href="https://backports.debian.org/Instructions/">Debian Backports</a>) will ensure that you always get the latest Tor stable, which will help ensure that the network is as secure and as reliable as can be.</p>
<p>If you need any help, please feel free to email the Network Health team at <a href="mailto:network-health@lists.torproject.org">network-health@lists.torproject.org</a>.</p>
<p>If you don't yet run a relay but want to, <a href="https://torproject.org/relay-guide">our guide</a> can help get you set up.</p>
<h2>Thank You, Relay Operators</h2>
<p>Support from relay operators is essential to keep the network healthy. Operators must keep their relays and machines up to date. Relays are the backbone of all software that relies on Tor, and each operator helps immensely in providing people with privacy and freedom online around the world. We cannot thank them enough.</p>
<p>Again and forever, a huge thank you to all relay operators out there that volunteer their time, resources, and effort to the Tor network. Your contributions are indispensable and everlasting.</p>
<p>The Tor Project</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/network">
          network
        </a>
      </li><li>
        <a href="../category/relays">
          relays
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-284349"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284349" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 08, 2019</p>
    </div>
    <a href="#comment-284349">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284349" class="permalink" rel="bookmark">Hurraaaay! Still not &lt;3…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hurraaaay! Still not &lt;3 police, but loving this kind of network police!</p>
<p>Seriously, this is a great and much awaited move from the Network team. </p>
<p>However I have a question: you have mentioned some possible (and very limited) drawbacks due to the removal of EOL relays. A part this, I am wondering if you have hypothesized the possible benefits the network could gain, or if you will just sit in front of your monitors and observe the situation, as if it was an experiment </p>
<p>(something like: "OK, this is a test  to get experience for the future. In this way, when we will decide to kick 0.2.8 relays off the network, we will already be prepared, we will already know what kind of problems can emerge and we will already know how to manage the situation").</p>
<p>Second question: how many relay operators answered to your call and the mail you sent them?</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284351"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284351" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>NoLimiteTeamTor (not verified)</span> said:</p>
      <p class="date-time">October 08, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284349" class="permalink" rel="bookmark">Hurraaaay! Still not &lt;3…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284351">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284351" class="permalink" rel="bookmark">It is probably pretty simple…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It is probably pretty simple logic.  For security for everyone the Version Firewall will protect everyone.  The only downside is that tor authority directories could become malicious but that is the same situation how it is established using Directory Authorities anyway so why not act out some police work against bad actors since the equation stays the same from the top. </p>
<p>You always have to "hypothesized the possible benefits" when maintaining the plumbing system.  It's a big world and the plumbing needs lots of TLC.  Nothing is perfect.  But improvement is the only outcome that is judged.  If no increase of logical pathways than the system breaks.  </p>
<p>Hope it helps!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284365"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284365" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dgoulet
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dgoulet said:</p>
      <p class="date-time">October 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284349" class="permalink" rel="bookmark">Hurraaaay! Still not &lt;3…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284365">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284365" class="permalink" rel="bookmark">Yes definitely we&#039;ve…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes definitely we've analysed the drawbacks of such a removal. As stated in the post, the benefits are in terms of security and stability mostly against loosing network capacity.</p>
<p>The more features we roll out, especially security ones, the more the network relays gets partitioned in cluster of relays supporting X or/and Y or/and Z.</p>
<p>Then, client picks those relays based on what they support and depending on what the client wants to do. For instance, the Tor circuit padding from client to Guard node, a client can not stop picking the other Guard nodes because they don't support padding. That would leak information about the client but also narrow down to a very small number of relays this client could pick and become an easier target. So to provide this new security feature to be used all the time, we need to wait that the entire network upgrade to at least a version supporting it. And that can be a long time...y</p>
<p>In other words, the more clusters of relays we have, the less the network is agile to provide new safety measures to everyone. And the more security issues linger in the network due to out dated and unmaintained versions.</p>
<p>And, this is not really an experiment to prepare us for the future although it was a learning experience for sure. We do reject relays regularly from the network to protect against attackers. Sometimes, it is large numbers.</p>
<p>For you second question, that is difficult to say without counting all the emails but I would say a reasonable number. Ideal world, they would all respond but some do upgrade without bothering to reply :).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284376"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284376" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to dgoulet</p>
    <a href="#comment-284376">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284376" class="permalink" rel="bookmark">Dear both,
thanks for your…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dear both,<br />
thanks for your insightful and detailed answers. I am fascinated by plumbing so, if you do not mind, I have a few more things I'd like to ask.</p>
<p>1) Was there a kind of "salient" and specific reason (like an attack to the network or something) that led the Network team to kick out EOL relays? Or it was more like: "Well, the topology of Tor is just becoming too partitioned and fragmented. These are the drawbacks, these are the benefits. Let's do this"? Or both?</p>
<p>2) On the organizational side, I was wondering... Why do you think this happens? It is difficult to provide an answer for me. I am not a coder or an hacker, but I know that anonymity and privacy rely on security. Hence, as bridge operator, I am very keen in checking the security of my nodes (compatibly with my technical skills which are not high). Actually, there is not much to do: once you set up unattended-upgrades and some tool for log audit a good part of the work is already done. So why? Is it just because people quickly setup a VPS with Debian (without using deb.torproject or backports) and forget about it? Is it negligence? Is it intentional? Or  do you have other ideas?</p>
<p>3) Directory Servers are charged with few but important technical functions (i.e. network topology and node state) that make Tor a distributed but semi-centralized network. This is no secret and the reasons for this choice are written in the Tor design paper. I am wondering... what else they do? Are they appointed with other technical functions? How did their design evolved since 2003/4? The real question is: is there one or more paper/link about this topic?</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-284354"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284354" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 09, 2019</p>
    </div>
    <a href="#comment-284354">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284354" class="permalink" rel="bookmark">Great move! I would actually…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great move! I would actually prefer to kick broken 0.2.9.x off too with changing their EOL date to now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284364"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284364" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dgoulet
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dgoulet said:</p>
      <p class="date-time">October 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284354" class="permalink" rel="bookmark">Great move! I would actually…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-284364">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284364" class="permalink" rel="bookmark">0.2.9.x series is EOL in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>0.2.9.x series is EOL in January 2020 so another 3 months. We'll simply roll that one up the end since expect maybe one other release, maybe two if something happens.</p>
<p>However, the 0.3.5.x LTS series, we will be rejecting every version up to 0.3.5.7 due to issues and alpha code.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-284370"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284370" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 09, 2019</p>
    </div>
    <a href="#comment-284370">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284370" class="permalink" rel="bookmark">I do not have an email…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I do not have an email account.  How can I report a possibly malfunctioning relay?</p>
<p>When the guard node assigned to your debian-tor instance (used for accessing the Debian onions), how can edit the torrc (which file exactly?) to choose a new guard?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284371"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284371" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>DontIgnoreBugReports (not verified)</span> said:</p>
      <p class="date-time">October 09, 2019</p>
    </div>
    <a href="#comment-284371">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284371" class="permalink" rel="bookmark">During the past week,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>During the past week, despite repeated attempts, I have been unable to report bugs to Tails Project using Whisperback (which emails their onion mail server).  Is it possible that the cause is owing to an EOL server not being properly maintained by Tails Project?</p>
<p>I have seen two types of error messages in multiple attempts: a TLS failure (I first guessed an expired certificate) and "Pysocks does not support IPv6".  In the first case in Onion Circuits I could see a failed attempt to contact an onion.  The the second case it appears that no circuits are being built.</p>
<p>I hope someone with a secure comms link with Tails Project will report this to them.  TIA.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284372"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284372" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 09, 2019</p>
    </div>
    <a href="#comment-284372">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284372" class="permalink" rel="bookmark">I am a user/donor not a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am a user/donor not a relay operator, but FWIW this sounds like a necessary step.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284379"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284379" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2019</p>
    </div>
    <a href="#comment-284379">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284379" class="permalink" rel="bookmark">Anyone know the state of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone know the state of health of the Debian onions?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284382"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284382" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2019</p>
    </div>
    <a href="#comment-284382">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284382" class="permalink" rel="bookmark">I didn&#039;t know the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I didn't know the Tor Project gave support to relays with old builds of the Tor software.<br />
I'm glad this decision was made, supporting many old builds only makes it harder for the developers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284418"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284418" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2019</p>
    </div>
    <a href="#comment-284418">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284418" class="permalink" rel="bookmark">Further questions that the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Further questions that the Network Health team should be asking:</p>
<ol>
<li>Why are those top most prevalent Obsolete or Unrecommended versions still being used?  Repository update lag?  Operator or packager neglect?  Discontinued devices or support? (as well as hardware such as Anonabox)  Malicious or researcher farms to exploit the network by way of vulnerabilities in old versions?  Something else?</li>
<li>What other ways can we encourage and facilitate their update and penalise dawdling?</li>
</ol>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284421"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284421" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>museumsoft (not verified)</span> said:</p>
      <p class="date-time">October 15, 2019</p>
    </div>
    <a href="#comment-284421">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284421" class="permalink" rel="bookmark">Why 0.2.4.x isn&#039;t affected?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why 0.2.4.x isn't affected?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-284506"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284506" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2019</p>
    </div>
    <a href="#comment-284506">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284506" class="permalink" rel="bookmark">&gt;Fingerprint is marked…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;Fingerprint is marked rejected -- if you think this is a mistake please set a valid email address in ContactInfo and send an email to <a href="mailto:bad-relays@lists.torproject.org" rel="nofollow">bad-relays@lists.torproject.org</a> mentioning your &gt;fingerprint(s)?<br />
you still do not have onions email. (!)</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
